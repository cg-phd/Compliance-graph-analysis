#include "Relaxation.h"
#include <cmath>
#include <algorithm>

#include "../util/util.h"

std::vector<double> LR(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambda)
{
    std::vector<double> adjusted = vals;

    // Which weight position we're at
    int ws_index = 0;
    for (std::vector<int> ws : weights)
    {   
        vecMult(ws, lambda[ws_index]);
        std::transform(adjusted.begin(), adjusted.end(), ws.begin(), adjusted.begin(), std::minus<double>());
        ws_index++;
    }

    return adjusted;
}

std::vector<double> antiLR(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambda)
{
    std::vector<double> adjusted = vals;

    // Which weight position we're at
    int ws_index = 0;
    for (std::vector<int> ws : weights)
    {   
        vecDiv(ws, lambda[ws_index]);
        std::transform(adjusted.begin(), adjusted.end(), ws.begin(), adjusted.begin(), std::plus<double>());
        ws_index++;
    }

    return adjusted;

}

void LR_solve(Knapsack &lr_sack, std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambdas)
{
    // Output suppression for debugging
    auto orig_buff = std::cout.rdbuf();
    int DEBUG = 0; // 1: Show cout, 0: Suppress
    if (DEBUG == 0)
       std::cout.rdbuf(nullptr);

    int ub = __INT_MAX__; // keep track of our LR upper bound

    double tol = 1e-8; // stop condition
    double ivl = 1.0; // starting ivl

    std::vector<double> last_slack(lambdas.size(), __DBL_MAX__);
    std::vector<double> last_vio(lambdas.size(), 0); // must be non-neg
    std::set<int> best_nodes;

    int loop_count = 0; // for analysis only

    int num_items = vals.size();
    std::cout << "Number of items:" << num_items << std::endl;

    // Lock each constraint to find best form of relaxation
    for(int w_it = 0; w_it < (int) weights.size(); w_it++)
    {
        if (w_it != 0)
        {
            std::deque<std::vector<int>>::iterator it = weights.end();
            std::vector<int> w_idx = weights.at(w_it);
            weights.erase(it);
            weights.emplace_front(w_idx);
            lr_sack.rotate_caps();
        }

        std::vector<int> w = weights.front(); // first weight is left unaffected by LR
        weights.pop_front();

        std::vector<double> last_lambdas = lambdas;

        while(ivl > tol)
        {
            int ub_flag = 0;
            std::cout << "Iteration: " << loop_count << std::endl;
            lr_sack.solve(w, LR(vals, weights, lambdas), num_items);
            int repair = 0;
            for (int i = 0; i < (int) weights.size(); i++)
            {
                repair += lr_sack.cap_limits()[i+1] * lambdas[i];
            }

            std::cout << "Lambdas: " << lambdas << std::endl;
            std::cout << "Pre-Repaired Knapsack Value: " << lr_sack.value() << std::endl;
            std::cout << "Repair Value: " << repair << std::endl;
            lr_sack.value(lr_sack.value() + repair);
            std::cout << "Knapsack Value: " << lr_sack.value() << std::endl;
            std::cout << "Chosen Items (IDs): " << lr_sack.node_ids() << std::endl;
            std::cout << "Weights: " << weights.front() << std::endl;
            std::cout << "Last Slack: " << last_slack << std::endl;
            std::cout << "Last Vio: " << last_vio << std::endl;

            if (lr_sack.value() < ub)
            {
                ub = lr_sack.value(); //update bound
                ub_flag = 1;
            }
            std::cout << "Upper Bound: " << ub << std::endl;

            //Compile Capacities
            std::set<int> items = lr_sack.node_ids();
            std::vector<int> curr_caps(weights.size(), 0);
            for (int item : items)
            {
                int j = 0;
                for (std::vector<int> w : weights)
                {
                    curr_caps[j] += w[item];
                    j++;
                }
            }

            //Update lambdas
            int j = 0;
            int equal = 0;
            for (int cap : curr_caps)
            {
                std::cout << "For the " << j << " idx constraint, the knapsack w is " << cap ;

                if (cap == lr_sack.cap_limits()[j+1]){
                    equal++;
                    std::cout << " which is equal to the limit of " << lr_sack.cap_limits()[j+1] << std::endl;
                    if (ub_flag == 1)
                        best_nodes = lr_sack.node_ids();
                }
                // Increase our lambda
                else if (cap > lr_sack.cap_limits()[j+1])
                {
                    // First iteration: Just increase to 1.0
                    if(lambdas[j] == 0.0)
                    {
                        last_lambdas[j] = lambdas[j];
                        lambdas[j] = 1.0;
                    }
                    else
                    {
                        // Update our vio if this is the greatest lambda we've found that violates the constraint
                        if (lambdas[j] > last_vio[j])
                            last_vio[j] = lambdas[j];

                        // Before we update, save our current lambda
                        last_lambdas[j] = lambdas[j];

                        // If we haven't found a slack yet, just double
                        if (last_slack[j] == __DBL_MAX__)
                            lambdas[j] = lambdas[j]*2.0;
                        else // midpoint between current lambda and our last known slack value
                        {
                            lambdas[j] = (lambdas[j]+last_slack[j])/2.0;
                            ivl = last_slack[j] - lambdas[j];
                        }

                    }
                    std::cout << " which is more than the limit of " << lr_sack.cap_limits()[j+1] << std::endl;

                }
                // Decrease our lambda
                else
                {
                    // If this is our lowest slack, set the value
                    if (lambdas[j] < last_slack[j])
                        last_slack[j] = lambdas[j];

                    if (last_vio[j] != 0)
                    {
                        last_lambdas[j] = lambdas[j];
                        lambdas[j] = (lambdas[j]+last_vio[j])/2.0;
                        ivl = lambdas[j] - last_vio[j];
                    }

                    else
                    {
                        double prev = last_lambdas[j];
                        last_lambdas[j] = lambdas[j];
                        lambdas[j] = (lambdas[j]+prev)/2.0;
                        ivl = lambdas[j] - prev;
                    }

                    if (ub_flag == 1)
                        best_nodes = lr_sack.node_ids();
                    std::cout << " which is less than the limit of " << lr_sack.cap_limits()[j+1] << std::endl;
                }
                j++;
            }

            // Optimal Found
            if (equal == (int) curr_caps.size())
            {
                lr_sack.op();
                break;
            }
            loop_count++;
            lr_sack.reset();
            std::cout << "IVL: " << ivl << std::endl;
            std::cout << "tol: " << tol << std::endl;
            std::cout << std::endl;
        }

        // Finalize knapsack
        lr_sack.reset(); 
        // Place best known node configuration
        for (int n : best_nodes)
            lr_sack.add_item(n);
        // Set best known value
        lr_sack.value(ub);
    }

    // Restore cout
    if (DEBUG == 0)
        std::cout.rdbuf(orig_buff);



}

void Relaxation::test_LR_solve(std::vector<double> vals, std::deque<std::vector<int>> weights)
{
    // Output suppression for debugging
    auto orig_buff = std::cout.rdbuf();
    int DEBUG = 1; // 1: Show cout, 0: Suppress
    if (DEBUG == 0)
       std::cout.rdbuf(nullptr);

    int ub = __INT_MAX__; // keep track of our LR upper bound

    double tol = 1e-8; // stop condition
    double ivl = 1.0; // starting ivl

    //std::vector<double> last_slack(lambdas.size(), __DBL_MAX__);
    //std::vector<double> last_vio(lambdas.size(), 0); // must be non-neg
    //std::set<int> best_nodes;

    //int loop_count = 0; // for analysis only

    int num_items = vals.size();
    std::cout << "Number of items:" << num_items << std::endl;

    // Lock each constraint to find best form of relaxation
    for(int w_it = 0; w_it < (int) weights.size(); w_it++)
    {
        if (w_it != 0)
        {
            std::deque<std::vector<int>>::iterator it = weights.end();
            std::vector<int> w_idx = weights.at(w_it);
            weights.erase(it);
            weights.emplace_front(w_idx);
            m_sack.rotate_caps();
        }

        std::vector<int> w = weights.front(); // first weight is left unaffected by LR
        weights.pop_front();

        //std::vector<double> last_lambdas = lambdas;

        while(ivl > tol)
        {
            int ub_flag = 0;
            std::cout << "Iteration: " << m_loop_count << std::endl;
            auto LRvals = LR(vals, weights, m_lambdas);
            for (double &val : LRvals)
            {
                if (val < 0)
                    val = 0;
            }
            m_sack.solve(w, LRvals, num_items);
            int repair = 0;
            for (int i = 0; i < (int) weights.size(); i++)
            {
                repair += m_sack.cap_limits()[i+1] * m_lambdas[i];
            }

            std::cout << "Lambdas: " << m_lambdas << std::endl;
            std::cout << "Pre-Repaired Knapsack Value: " << m_sack.value() << std::endl;
            std::cout << "Repair Value: " << repair << std::endl;
            m_sack.value(m_sack.value() + repair);
            std::cout << "Knapsack Value: " << m_sack.value() << std::endl;
            std::cout << "Chosen Items (IDs): " << m_sack.node_ids() << std::endl;
            std::vector<int> chosen_lr_vals;
            for (int n : m_sack.node_ids())
                chosen_lr_vals.emplace_back(LRvals[n]);
            //std::cout << "Chosen node vals:" << chosen_lr_vals << std::endl;
            //std::cout << "Weights: " << weights << std::endl;
            std::cout << "Last Slack: " << m_last_slack << std::endl;
            std::cout << "Last Vio: " << m_last_vio << std::endl;

            if (m_sack.value() < ub)
            {
                ub = m_sack.value(); //update bound
                ub_flag = 1;
            }
            std::cout << "Upper Bound: " << ub << std::endl;

            //Compile Capacities
            std::set<int> items = m_sack.node_ids();
            std::vector<int> curr_caps(weights.size(), 0);
            for (int item : items)
            {
                int j = 0;
                for (std::vector<int> l : weights)
                {
                    curr_caps[j] += l[item];
                    j++;
                }
            }

            //Update lambdas
            int j = 0;
            int equal = 0;
            for (int cap : curr_caps)
            {
                std::cout << "For the " << j << " idx constraint, the knapsack w is " << cap ;

                if (cap == m_sack.cap_limits()[j+1]){
                    equal++;
                    std::cout << " which is equal to the limit of " << m_sack.cap_limits()[j+1] << std::endl;
                    if (ub_flag == 1)
                        m_best_nodes = m_sack.node_ids();
                }
                // Increase our lambda
                else if (cap > m_sack.cap_limits()[j+1])
                {
                    // First iteration: Just increase to 1.0
                    if(m_lambdas[j] == 0.0)
                    {
                        m_last_lambdas[j] = m_lambdas[j];
                        m_lambdas[j] = 1.0;
                    }
                    else
                    {
                        // Update our vio if this is the greatest lambda we've found that violates the constraint
                        if (m_lambdas[j] > m_last_vio[j])
                            m_last_vio[j] = m_lambdas[j];

                        // Before we update, save our current lambda
                        m_last_lambdas[j] = m_lambdas[j];

                        // If we haven't found a slack yet, just double
                        if (m_last_slack[j] == __DBL_MAX__)
                            m_lambdas[j] = m_lambdas[j]*2.0;
                        else // midpoint between current lambda and our last known slack value
                        {
                            m_lambdas[j] = (m_lambdas[j]+m_last_slack[j])/2.0;
                            ivl = m_last_slack[j] - m_lambdas[j];
                        }

                    }
                    std::cout << " which is more than the limit of " << m_sack.cap_limits()[j+1] << ", so our lambdas were increased from " << m_last_lambdas << " to " << m_lambdas << std::endl;

                }
                // Decrease our lambda
                else
                {
                    // If this is our lowest slack, set the value
                    if (m_lambdas[j] < m_last_slack[j])
                        m_last_slack[j] = m_lambdas[j];

                    if (m_last_vio[j] != 0)
                    {
                        m_last_lambdas[j] = m_lambdas[j];
                        m_lambdas[j] = (m_lambdas[j]+m_last_vio[j])/2.0;
                        ivl = m_lambdas[j] - m_last_vio[j];
                    }

                    else
                    {
                        double prev = m_last_lambdas[j];
                        m_last_lambdas[j] = m_lambdas[j];
                        m_lambdas[j] = (m_lambdas[j]+prev)/2.0;
                        ivl = m_lambdas[j] - prev;
                    }

                    if (ub_flag == 1)
                        m_best_nodes = m_sack.node_ids();
                    std::cout << " which is less than the limit of " << m_sack.cap_limits()[j+1] << std::endl;
                }
                j++;
            }

            // Optimal Found
            if (equal == (int) curr_caps.size())
            {
                m_sack.op();
                break;
            }
            m_loop_count++;
            //lr_sack.reset();
            std::cout << "IVL: " << ivl << std::endl;
            std::cout << "tol: " << tol << std::endl;
            std::cout << "lambdas: " << m_lambdas << std::endl;
            std::cout << std::endl;
            
            std::cout << "Break 1" << std::endl;
            break;
        }

        // Finalize knapsack
        //lr_sack.reset(); 
        // Place best known node configuration
        for (int n : m_best_nodes)
            m_sack.add_item(n);
        // Set best known value
        m_sack.value(ub);

        std::cout << "Break 2" << std::endl;
        break;
    }

    // Restore cout
    if (DEBUG == 0){
        std::cout.rdbuf(orig_buff);

    }    
    
    std::cout << "Returning" << std::endl;
    return;

}

void Relaxation::iterate(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> antisevs)
{
    int ub_flag = 0;
    int num_items = vals.size();
    double ivl = 1.0; // starting ivl
    //std::cout << "Weights: " << weights << std::endl;
    std::vector<int> locked = weights.front();
    weights.pop_front();
    //m_sack.solve(locked, LR(vals, weights, m_lambdas), num_items-1);
    
    m_sack.solve(locked, vals, num_items-1);

    int repair = 0;
    for (int i = 0; i < (int) weights.size(); i++)
    {
        repair += m_sack.cap_limits()[i+1] * m_lambdas[i];
    }

    m_sack.value(m_sack.value() + repair);
    //if (m_sack.value() < m_ub)
    //{
    //    m_ub = m_sack.value(); //update bound
    //    ub_flag = 1;
    //}
    

    //weights.emplace_front(locked);
    //Compile Capacities
    std::set<int> items = m_sack.node_ids(); // Get Nodes we've added to the knapsack
    //std::cout << "Added to sack: " << m_sack.node_ids() << std::endl;
    std::vector<int> curr_caps(weights.size(), 0); // Temporary cap tracker: do NOT pull from m_sack.curr_caps yet: MDKP works with 1D weights only - it has no idea what other dimensions are -> Add them up here
    for (int item : items)
    {
        for (int i = 0; i < (int) weights.size(); i++)
        {
            curr_caps[i] += weights[i][item];
        }
    }

    //Update lambdas
    int j = 0;
    int equal = 0;
    for (int cap : curr_caps)
    {
        if (cap == m_sack.cap_limits()[j+1]){
            equal++;
            //if (ub_flag == 1)
                m_best_nodes = m_sack.node_ids();
                m_sack.op(true);
                return;
        }
        // Increase our lambda
        else if (cap > m_sack.cap_limits()[j+1])
        {
            // First iteration: Just increase to 1.0
            if(m_lambdas[j] == 0.0)
            {
                if((int) m_last_lambdas.size() <= j)
                    m_last_lambdas.emplace_back(m_lambdas[j]);
                else
                    m_last_lambdas[j] = m_lambdas[j];
                
                m_lambdas[j] = 1.0;
            }
            else
            {
                // Update our vio if this is the greatest lambda we've found that violates the constraint
                if (m_lambdas[j] > m_last_vio[j])
                    m_last_vio[j] = m_lambdas[j];

                // Before we update, save our current lambda
                m_last_lambdas[j] = m_lambdas[j];

                // If we haven't found a slack yet, just double
                if (m_last_slack[j] == __DBL_MAX__)
                    m_lambdas[j] = m_lambdas[j]*2.0;
                else // midpoint between current lambda and our last known slack value
                {
                    m_lambdas[j] = (m_lambdas[j]+m_last_slack[j])/2.0;
                    ivl = std::abs(m_last_slack[j] - m_lambdas[j]);
                }
                // NOTE: not updating best_nodes here even if ub_flag is set. Because we've violated our constraints, this is NOT a feasible solution.
                // We cannot set our best nodes this iteration.
            }
        }
        // Decrease our lambda
        else
        {   
            std::cout << "Feasible solution found with value = " << m_sack.value() << " . Lambdas used were: " << m_lambdas <<  std::endl;
            std::cout << m_sack.node_ids() << std::endl;
            std::cout << "Knapsack limits: " << m_sack.curr_cap_status() << std::endl;
            std::vector<double> realvals = antiLR(antisevs, weights, m_lambdas);
            //std::vector<double> realweights = weights;
            //vecDiv(realweights, lambdas);

            double real_val = 0.0;
            for (int id : m_sack.node_ids())
            {
                real_val += realvals[id];
            }
            std::cout << "Real Value:" << real_val << std::endl;
            std::cout << std::endl;

            m_ub = m_sack.value();
            m_best_nodes = m_sack.node_ids();
            m_best_lambdas = m_lambdas;

            if (m_last_lambdas[j] == m_lambdas[j])
            {
                // We've hit min
                std::cout << "Stuck at min: not running into any cap constraints." << std::endl;
                m_stop = true;
                return;
            }

            // If this is our lowest slack, set the value
            if (m_lambdas[j] < m_last_slack[j])
                m_last_slack[j] = m_lambdas[j];

            if (m_last_vio[j] != 0)
            {
                m_last_lambdas[j] = m_lambdas[j];
                m_lambdas[j] = (m_lambdas[j]+m_last_vio[j])/2.0;
                ivl = std::abs(m_lambdas[j] - m_last_vio[j]);
            }

            else
            { 
                int ivl_flag = 0;
                // First iteration check -> avoid FPEs
                if (m_last_lambdas[j] == 0 && m_lambdas[j] == 0)
                {
                    ivl_flag = 1;
                }

                double prev = m_last_lambdas[j];
                m_last_lambdas[j] = m_lambdas[j];
                m_lambdas[j] = (m_lambdas[j]+prev)/2.0;

                if(ivl_flag == 1){
                    ivl =  0.5;
                }
                else{
                    ivl = std::abs(m_lambdas[j] - prev);
                }
            }

            if (ub_flag == 1){
                m_best_nodes = m_sack.node_ids();
                m_best_lambdas = m_last_lambdas;
            }

        }
        j++;
    }

    m_loop_count++;
    //m_sack.reset();

    // Optimal Found
    if (equal == (int) curr_caps.size())
    {
        //std::cout << "Optimal solution found." << std::endl;
        //std::cout << m_sack.node_ids() << std::endl;
        //m_sack.op(true);
    }

    if(ivl < m_tol){
        m_stop = true;   
    }
}

void Relaxation::reset()
{
    m_ub = INT_MAX;
    m_loop_count = 0;
    m_stop = false;
    m_lambdas = { (double) (m_sack.cap_limits().size()-1), 0 };
    m_last_lambdas = { (double) (m_sack.cap_limits().size()-1), 0 };
    m_last_slack = { (double) (m_sack.cap_limits().size()-1), DBL_MAX };
    m_last_vio = { (double) (m_sack.cap_limits().size()-1), 0 };
    m_best_nodes.clear();
    m_mitigated_exploits.clear();
    m_best_lambdas.clear();
    
    m_sack.reset();
}