#include <iostream>
#include <iterator> 
#include <numeric>

#include "Node.h"
#include "../util/util.h"

void Node::print() const
{
    std::cout << "Node: " << m_id << std::endl;
    std::cout << "  Severity: " << m_severity << std::endl;
    for (const Exploit &e : m_exps){
        e.print();
    }
    
    std::cout << "" << std::endl;
}

void Node::collapse_exploits(std::vector<double> lambdas, int num_constraints)
{
    // Sum of all Exploits' costs for the node, initialized to {num_constraints} * 0
    std::vector<int> costs = {num_constraints, 0}; 
    bool global_flag = true;

    if (m_exps.size() == 1 && (*(m_exps.begin())).id() == -1)
    {
        m_exps.erase(m_exps.begin());
    }
     
    if (m_exps.size() == 0)
    {
        Exploit collapsed_e(-1);
        std::vector<int> unprev_costs = {num_constraints, __INT_MAX__};
        Mitigation collapsed_mit(-1, unprev_costs, false);
        collapsed_e.add_mit(collapsed_mit);
        this -> add_exploit(collapsed_e);
        return;
    }

    else
    {
        std::set<Exploit>::iterator it = m_exps.begin();
        while(it  != m_exps.end())
        {
            Exploit e = *it;
            if (e.id() != -1)
            {
                // Obtain the "best" Mitigation for the exploit
                e.reduce_mits(m_severity, lambdas);

                // Add the costs of that Mitigation to the Node's cost sum
                if (e.is_preventable()){
                    vecAdd(costs, e.mits()[0].costs());
                }
                else{
                    //vecAdd(costs, unprev_costs);
                    //costs = {__INT_MAX__};
                }
                global_flag = global_flag  && e.mits()[0].is_global_mit();
                it++;
            }
            else
            {
                it = m_exps.erase(it);
            }
            //it++;
        }

        // Make an exploit with ID -1 (Node Exploit's are in a set - will be first item)
        Exploit collapsed_e(-1);
        // Add a Mitigation that has costs equal to the sum of all Exploits
        Mitigation collapsed_mit(-1, costs, global_flag);
        collapsed_e.add_mit(collapsed_mit);

        this -> add_exploit(collapsed_e);
    }


}

std::vector<int> Node::costs() const
{
    std::vector<int> total_c = {0};
    for (Exploit e : m_exps)
    {
        if (e.id() != -1 && e.mits().size() > 0){
            vecAdd(total_c, e.mits().front().costs());
        }
    }

    return total_c;
}

std::vector<double> Node::min_damages() const
{
    std::vector<double> retdamages = {0};
    int cost = __INT_MAX__;

    for (Exploit e : m_exps)
    {
        std::vector<double> damages = e.damages();
        if (std::accumulate(damages.begin(), damages.end(), decltype(damages)::value_type(0)) < cost)
            retdamages = damages;
    }

    return retdamages;
}
        
std::vector<double> Node::max_damages() const
{
    std::vector<double> retdamages = {0};
    int cost = __INT_MAX__;

    for (Exploit e : m_exps)
    {
        std::vector<double> damages = e.damages();
        if (std::accumulate(damages.begin(), damages.end(), decltype(damages)::value_type(0)) > cost)
            retdamages = damages;
    }

    return retdamages;
}
