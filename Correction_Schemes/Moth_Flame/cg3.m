%% 3D CG
function f = cg3 (x)
% Number of objective is 2
% Number of variables is 30. Range x [0,1]
f = [];
n=length(x);

f(1)=300+2*1*sum(x(2:n));
f(2)=5000+(5000/30)*sum(x(2:n));
%f(1)=5000+(5000/30)*sum(x(2:n));
%f(2)=300+2*1*sum(x(2:n));

