graph [
  directed 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "415"
  ]
  node [
    id 1
    label "405"
  ]
  node [
    id 2
    label "417"
  ]
  node [
    id 3
    label "404"
  ]
  node [
    id 4
    label "414"
  ]
  node [
    id 5
    label "419"
  ]
  node [
    id 6
    label "416"
  ]
  node [
    id 7
    label "407"
  ]
  node [
    id 8
    label "411"
  ]
  node [
    id 9
    label "412"
  ]
  node [
    id 10
    label "406"
  ]
  node [
    id 11
    label "408"
  ]
  node [
    id 12
    label "418"
  ]
  node [
    id 13
    label "403"
  ]
  node [
    id 14
    label "402"
  ]
  node [
    id 15
    label "409"
  ]
  node [
    id 16
    label "420"
  ]
  node [
    id 17
    label "401"
  ]
  node [
    id 18
    label "413"
  ]
  node [
    id 19
    label "410"
  ]
  node [
    id 20
    label "421"
  ]
  edge [
    source 0
    target 20
    label "21"
  ]
  edge [
    source 3
    target 10
    label "20"
  ]
  edge [
    source 5
    target 16
    label "20"
  ]
  edge [
    source 6
    target 12
    label "20"
  ]
  edge [
    source 7
    target 8
    label "21"
  ]
  edge [
    source 7
    target 0
    label "20"
  ]
  edge [
    source 8
    target 20
    label "20"
  ]
  edge [
    source 10
    target 20
    label "21"
  ]
  edge [
    source 11
    target 8
    label "21"
  ]
  edge [
    source 11
    target 12
    label "20"
  ]
  edge [
    source 12
    target 20
    label "21"
  ]
  edge [
    source 15
    target 8
    label "21"
  ]
  edge [
    source 16
    target 20
    label "21"
  ]
  edge [
    source 17
    target 10
    label "20"
  ]
  edge [
    source 17
    target 8
    label "21"
  ]
  edge [
    source 18
    target 0
    label "20"
  ]
  edge [
    source 19
    target 8
    label "21"
  ]
  edge [
    source 19
    target 16
    label "20"
  ]
]
