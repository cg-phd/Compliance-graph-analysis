#pragma once

int object_creation_test();
int base_knapsack_test();
int lr_test();
int ps_test();
int mitigation_reduction_test();
int exploit_collapse_test();
