#!/usr/bin/python3

import networkx as nx
import matplotlib.pyplot as plt
from collections import OrderedDict
from operator import getitem
import itertools
import configparser

def perc_cent(dotfile, pkn):
  config = configparser.ConfigParser()
  config.read(pkn)
  vio_ed = [] ## vio_ed is list where each entry is an edge label (as str) that causes a violation
  num_exploits = config['General']['num_exploits']
  warning_steps = 2

  print("Parsing", num_exploits, "exploits in the PKN.", flush = True)
  for e in range(int(num_exploits)):
    if config[("Exploit "+str(e))]['causes_vio'] == 'true':
      vio_ed.append(str(e))
    elif "redirect" in config[("Exploit "+str(e))]['causes_vio']:
      vio_ed.append(str(e))
  print("Parsed.", flush = True)
  
  G = nx.drawing.nx_agraph.read_dot(dotfile)
  if(G.has_node('\\n')):
    G.remove_node('\\n') # Remove "newline" node from newline end of dot file
  print("Graph imported.", flush = True)
  
  # Get list of ALL seen exploits (edge labels) - only unique values
  elabels = nx.get_edge_attributes(G, "label") # keep ALL seen labels - will need them later
  exploits = list(set(elabels.values())) # only need the unique values for exploits
  exploits.sort() # don't really need to sort. doesn't matter. unique e << edges, so low runtime anyway
  print("Gathered edge labels.", flush = True)
  
  # Get node IDs for each exploit
  ## vio_ed is list, where '' is an edge label (as str) and eid is the infection id (exploit id)
  vio_nodes = set()
  for k,v in elabels.items():
    if (v in vio_ed or str(v) in vio_ed):
      vio_nodes.add(k[1])
  
  with open('./vio.txt', 'w') as outfile:
    outfile.write('\n'.join(str(i) for i in vio_nodes))
  print("Gathered nodes that are in violation.", flush = True)

  exposed_nodes = set()
  for node in vio_nodes:
    # Get all nodes that can reach the infected nodes
    e_dict = nx.shortest_path_length(G,target=node)
    # reduce to the reachability step - exclude itself (0) and infected nodes
    exposed_nodes.update([k for k,v in e_dict.items() if (v in range(1,warning_steps) and k not in vio_nodes)])
    
  # Write to file: will reuse this
  with open('./expo.txt', 'w') as outfile:
    outfile.write('\n'.join(str(i) for i in exposed_nodes))

  print("Gathered exposed nodes.", flush = True)

  # Initialize all to 0.01
  nx.set_node_attributes(G, 0.01, 'percolation')
  for node, data in G.nodes(data=True):
    if str(node) in vio_nodes:
      data['percolation'] = 0.99
    elif str(node) in exposed_nodes:
      data['percolation'] = 0.50


  print("Added weights to all nodes.", flush = True)
  perc_d = nx.percolation_centrality(G, attribute="percolation")

  return perc_d

