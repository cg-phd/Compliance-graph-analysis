#!/bin/bash
for i in 3 50 100 150 200 250 300 350 400 450 501 550 600 650;
do
	cat ../${i}hipaa.csv >> final_hipaa.csv
done

sed -i '/Monetary,Time,DG,ROIM,ROIT/d' final_hipaa.csv
sed -i 1i"Monetary,Time,DG,ROIM,ROIT" final_hipaa.csv
