import pandas as pd
from patsy import dmatrices
from sklearn.model_selection import train_test_split

import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt


df = pd.read_csv('car_ft.csv', header=0)
#df = pd.read_csv('osha_ft.csv', header=0)
#df = pd.read_csv('hipaa_ft.csv', header=0)

#df_train = df[df['0_CT']>0]
#df_test = df

#OSHA
#dep_var = '27_CT'
#dep_var = 'E_1'
#CAR
dep_var = '25_CT'

#HIPAA
#dep_var = '26_CT'
#dep_var = 'mu_1'

indep_cols = df.columns.values.tolist()
indep_cols.remove(dep_var)

df_train, df_test = train_test_split(df, test_size=0.20, random_state=30)
#df_train, df_test = train_test_split(df, test_size=0.80)



y_train = df_train[dep_var]
y_test = df_test[dep_var]
X_train = df_train[indep_cols]
X_test = df_test[indep_cols]

print('Training data set length='+str(len(df_train)))
print('Testing data set length='+str(len(df_test)))

zip_training_results = sm.ZeroInflatedPoisson(endog=y_train, exog=X_train, exog_infl=X_train, inflation='logit').fit()

print(zip_training_results.summary())

zip_predictions = zip_training_results.predict(X_test,exog_infl=X_test)
predicted_counts=np.round(zip_predictions)
actual_counts = y_test
print(actual_counts)
print(predicted_counts)
print('ZIP RMSE='+str(np.sqrt(np.sum(np.power(np.subtract(predicted_counts,actual_counts),2)))))

fig = plt.figure()
fig.suptitle('Predicted vs Actual Counts Using the ZIP Model for the Automotive Maintenance Network for Edge ID 25')
#predicted, = plt.plot(X_test.index, predicted_counts, 'go-', label='Predicted')
#actual, = plt.plot(X_test.index, actual_counts, 'ro-', label='Actual')
predicted, = plt.plot(list(predicted_counts), 'go-', label='Predicted')
actual, = plt.plot(list(actual_counts), 'ro-', label='Actual')
plt.legend(handles=[predicted, actual])
plt.xlabel("Time Step")
plt.ylabel("Edge Count")
plt.show()