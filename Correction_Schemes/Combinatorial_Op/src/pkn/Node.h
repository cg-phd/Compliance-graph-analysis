#pragma once

#include "Exploit.h"
#include <set>

class Node
{
    public:
        Node(int id, double severity, Exploit e):
            m_id { id },
            m_severity { severity },
            m_exps { e },
            m_damages ( 2, 0.0 )
        {}

        Node(int id, double severity):
            m_id { id },
            m_severity { severity },
            m_damages ( 2, 0.0 )
        {}

        // ID getter/setter
        int id() const { return m_id; }
        void id(int i ) { m_id = i; }

        // Severity getter/setter
        double severity() const { return m_severity; }
        void severity(double sev) { m_severity = sev; }

        std::set<Exploit> exploits() const { return m_exps; }
        void exploits(std::set<Exploit> es) { m_exps = es; } //overwrite (not update) exploits with passed exploit set
        void add_exploit(Exploit ne) { m_exps.insert(ne); }
        std::set<Exploit>::iterator remove_exploit(std::set<Exploit>::iterator it) { return m_exps.erase(it); }

        void collapse_exploits(std::vector<double> lambdas, int num_constraints);
        
        std::vector<double> min_damages() const;
        std::vector<double> max_damages() const;

        std::vector<double> damages() const { return m_damages; }
        void damages(std::vector<double> d) { m_damages = d; }
        std::vector<int> costs() const;

        void print() const;

        bool operator<  (const Node rhs) const { return (m_id < rhs.m_id); }
        bool operator>  (const Node rhs) const { return (m_id > rhs.m_id); }
        bool operator<= (const Node rhs) const { return (m_id <= rhs.m_id); }
        bool operator>= (const Node rhs) const { return (m_id >= rhs.m_id); }
        bool operator== (const Node rhs) const { return (m_id == rhs.m_id && m_severity == rhs.m_severity && m_exps == rhs.m_exps); }

    private:
        int m_id;
        double m_severity;
        std::set<Exploit> m_exps;
        std::vector<double> m_damages;
};