function plot_raw(f1,f2)
X=f1;
Y=f2;
n=length(Z);
c=linspace(65,100,n);
figure;
scatter(X, Y,30,c,'o', 'filled');
view(-30, 20);
% Add title and axis labels

title('Optimal Solution Pareto Set');
xlabel('Objective function value 1');
ylabel('Objective function value 2');
end
