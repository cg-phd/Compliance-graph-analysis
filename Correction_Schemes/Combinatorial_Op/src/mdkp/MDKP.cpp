#include <bits/stdc++.h>
#include <climits>
#include <iostream>

#include "../util/util.h"
#include "MDKP.h"

void Knapsack::print()
{
    std::cout << "Knapsack Max Capacities: " << m_cap_limits << std::endl;
    std::cout << "Knapsack Value: " << m_value << std::endl;
    std::cout << "Current Capacities: " << m_curr_caps << std::endl;
    std::cout << "Knapsack Node IDs Mitigated: " << m_node_ids << std::endl;
}

void Knapsack::solve(std::vector<int> weights, std::vector<double> vals, int n)
{
    int W = m_cap_limits.front();

    //value table
    std::vector<std::vector<double>> K(n+1, std::vector<double>(W+1, 0));
 
    // Build table K[][] in bottom up manner
    for (int i = 0; i <= n; i++) {
        for (int w = 0; w <= W; w++) {
            if (i == 0 || w == 0){
                K[i][w] = 0;
            }
            else if (weights[i-1] <= w){
                K[i][w] = knap_max(vals[i-1]+K[i-1][w-weights[i-1]], K[i-1][w]);
            }
            else{
                K[i][w] = K[i-1][w];
            }
        }
    }
    m_value = K[n][W];
         
    int w = W;
    double tracker = K[n][W];

    const auto relative_difference_factor = 0.0001;    // 0.01%

    for (int i = n; i > 0 && tracker > 0; i--) 
    {     
        //const auto greater_magnitude = std::max(std::abs(tracker),std::abs(K[i-1][w]));
        //if ( std::abs(tracker-K[i-1][w]) < relative_difference_factor * greater_magnitude )
        if (tracker == K[i-1][w])
            continue;    
 
        else //item included
        { 
            this -> add_item(i-1);
            tracker -= vals[i-1];
            w -= weights[i-1];
        }
    }

}

void Knapsack::reset()
{
    m_value = 0;
    m_optimal_found = false;
    m_node_ids.erase(m_node_ids.begin(), m_node_ids.end());
}

//Shift right
void Knapsack::rotate_caps()
{
    // Get current capacities
    std::vector<int> old_caps = m_cap_limits;

    // Get the last index so we can loop/ring it to the front
    int c_idx = old_caps.at(old_caps.size()-1);

    // Erase the last element and loop/ring it to the front
    old_caps.erase(old_caps.end());
    old_caps.insert(old_caps.begin(), c_idx);

    // Update capacity member
    m_cap_limits = old_caps;
}
