#pragma once

#include <vector>

class Mitigation
{
    public:
        Mitigation(int id, std::vector<int> costs, bool is_global):
            m_id { id },
            m_costs { costs },
            m_is_global { is_global }
        {}

        // ID getter/setters
        int id() const { return m_id; }
        void id(int i) { m_id = i; }

        // Cost getters/setters
        std::vector<int> costs() const { return m_costs; }
        void costs(std::vector<int> new_c) { m_costs = new_c; } //overwrite (not update) old c with passed c

        // Global getter/setters
        bool is_global_mit() { return m_is_global; }
        void is_global_mit(bool is_global) { m_is_global = is_global; }

        void print() const;

        /*
            Return a value given a value (severity), Lagrangian Relaxation penalties, and cost constraints
            Used to identify "best" Mitigation in a set of Mitigations
             Note: Since all Mitigations will have the same constraints (cancels out) - can simplify the LR by removing the constraint consideration
        */
        double lr(int vals, std::vector<double> lambdas);
        
        bool operator<  (const Mitigation rhs) const { return (m_id < rhs.m_id); }
        bool operator>  (const Mitigation rhs) const { return (m_id > rhs.m_id); }
        bool operator<= (const Mitigation rhs) const { return (m_id <= rhs.m_id); }
        bool operator>= (const Mitigation rhs) const { return (m_id >= rhs.m_id); }
        bool operator== (const Mitigation rhs) const { return (m_id == rhs.m_id && m_costs == rhs.m_costs && m_is_global == rhs.m_is_global); }

    private:
        int m_id;
        std::vector<int> m_costs;
        bool m_is_global; // If this Mitigation is implemented, can it be "re-used" in other Exploit preventions?
};