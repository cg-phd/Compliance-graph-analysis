graph [
  directed 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "72"
  ]
  node [
    id 1
    label "51"
  ]
  node [
    id 2
    label "5"
  ]
  node [
    id 3
    label "6"
  ]
  node [
    id 4
    label "29"
  ]
  node [
    id 5
    label "8"
  ]
  node [
    id 6
    label "24"
  ]
  node [
    id 7
    label "58"
  ]
  node [
    id 8
    label "62"
  ]
  node [
    id 9
    label "11"
  ]
  node [
    id 10
    label "70"
  ]
  node [
    id 11
    label "27"
  ]
  node [
    id 12
    label "36"
  ]
  node [
    id 13
    label "4"
  ]
  node [
    id 14
    label "1"
  ]
  node [
    id 15
    label "40"
  ]
  node [
    id 16
    label "13"
  ]
  node [
    id 17
    label "2"
  ]
  node [
    id 18
    label "60"
  ]
  node [
    id 19
    label "9"
  ]
  node [
    id 20
    label "28"
  ]
  node [
    id 21
    label "3"
  ]
  node [
    id 22
    label "31"
  ]
  node [
    id 23
    label "39"
  ]
  node [
    id 24
    label "43"
  ]
  node [
    id 25
    label "19"
  ]
  node [
    id 26
    label "46"
  ]
  node [
    id 27
    label "47"
  ]
  node [
    id 28
    label "22"
  ]
  node [
    id 29
    label "48"
  ]
  node [
    id 30
    label "57"
  ]
  node [
    id 31
    label "69"
  ]
  node [
    id 32
    label "45"
  ]
  node [
    id 33
    label "63"
  ]
  node [
    id 34
    label "42"
  ]
  node [
    id 35
    label "44"
  ]
  node [
    id 36
    label "59"
  ]
  node [
    id 37
    label "49"
  ]
  node [
    id 38
    label "67"
  ]
  node [
    id 39
    label "23"
  ]
  node [
    id 40
    label "16"
  ]
  node [
    id 41
    label "17"
  ]
  node [
    id 42
    label "56"
  ]
  node [
    id 43
    label "14"
  ]
  node [
    id 44
    label "30"
  ]
  node [
    id 45
    label "35"
  ]
  node [
    id 46
    label "52"
  ]
  node [
    id 47
    label "65"
  ]
  node [
    id 48
    label "7"
  ]
  node [
    id 49
    label "54"
  ]
  node [
    id 50
    label "68"
  ]
  node [
    id 51
    label "10"
  ]
  node [
    id 52
    label "25"
  ]
  node [
    id 53
    label "66"
  ]
  node [
    id 54
    label "12"
  ]
  node [
    id 55
    label "50"
  ]
  node [
    id 56
    label "15"
  ]
  node [
    id 57
    label "64"
  ]
  node [
    id 58
    label "33"
  ]
  node [
    id 59
    label "26"
  ]
  node [
    id 60
    label "55"
  ]
  node [
    id 61
    label "34"
  ]
  node [
    id 62
    label "71"
  ]
  node [
    id 63
    label "53"
  ]
  node [
    id 64
    label "61"
  ]
  node [
    id 65
    label "32"
  ]
  node [
    id 66
    label "41"
  ]
  node [
    id 67
    label "37"
  ]
  node [
    id 68
    label "18"
  ]
  node [
    id 69
    label "21"
  ]
  node [
    id 70
    label "38"
  ]
  node [
    id 71
    label "20"
  ]
  edge [
    source 1
    target 47
    label "17"
  ]
  edge [
    source 2
    target 19
    label "17"
  ]
  edge [
    source 2
    target 43
    label "22"
  ]
  edge [
    source 2
    target 68
    label "26"
  ]
  edge [
    source 2
    target 69
    label "12"
  ]
  edge [
    source 2
    target 28
    label "30"
  ]
  edge [
    source 2
    target 39
    label "31"
  ]
  edge [
    source 3
    target 9
    label "17"
  ]
  edge [
    source 3
    target 56
    label "22"
  ]
  edge [
    source 3
    target 25
    label "26"
  ]
  edge [
    source 3
    target 28
    label "28"
  ]
  edge [
    source 3
    target 6
    label "31"
  ]
  edge [
    source 4
    target 7
    label "9"
  ]
  edge [
    source 4
    target 8
    label "28"
  ]
  edge [
    source 4
    target 57
    label "29"
  ]
  edge [
    source 4
    target 47
    label "30"
  ]
  edge [
    source 5
    target 52
    label "9"
  ]
  edge [
    source 5
    target 59
    label "28"
  ]
  edge [
    source 5
    target 11
    label "29"
  ]
  edge [
    source 5
    target 20
    label "30"
  ]
  edge [
    source 5
    target 4
    label "31"
  ]
  edge [
    source 6
    target 12
    label "17"
  ]
  edge [
    source 6
    target 35
    label "22"
  ]
  edge [
    source 6
    target 1
    label "26"
  ]
  edge [
    source 6
    target 49
    label "28"
  ]
  edge [
    source 9
    target 20
    label "26"
  ]
  edge [
    source 9
    target 65
    label "28"
  ]
  edge [
    source 9
    target 61
    label "29"
  ]
  edge [
    source 9
    target 12
    label "31"
  ]
  edge [
    source 11
    target 42
    label "9"
  ]
  edge [
    source 11
    target 18
    label "28"
  ]
  edge [
    source 11
    target 33
    label "30"
  ]
  edge [
    source 11
    target 57
    label "31"
  ]
  edge [
    source 12
    target 47
    label "26"
  ]
  edge [
    source 12
    target 62
    label "28"
  ]
  edge [
    source 12
    target 0
    label "29"
  ]
  edge [
    source 13
    target 5
    label "17"
  ]
  edge [
    source 13
    target 16
    label "22"
  ]
  edge [
    source 13
    target 41
    label "9"
  ]
  edge [
    source 13
    target 68
    label "28"
  ]
  edge [
    source 13
    target 25
    label "30"
  ]
  edge [
    source 13
    target 71
    label "31"
  ]
  edge [
    source 14
    target 17
    label "17"
  ]
  edge [
    source 14
    target 21
    label "22"
  ]
  edge [
    source 14
    target 13
    label "26"
  ]
  edge [
    source 14
    target 2
    label "28"
  ]
  edge [
    source 14
    target 3
    label "30"
  ]
  edge [
    source 14
    target 48
    label "31"
  ]
  edge [
    source 15
    target 4
    label "17"
  ]
  edge [
    source 16
    target 5
    label "17"
  ]
  edge [
    source 16
    target 67
    label "9"
  ]
  edge [
    source 16
    target 70
    label "28"
  ]
  edge [
    source 16
    target 23
    label "30"
  ]
  edge [
    source 16
    target 15
    label "31"
  ]
  edge [
    source 17
    target 5
    label "26"
  ]
  edge [
    source 17
    target 19
    label "28"
  ]
  edge [
    source 17
    target 51
    label "29"
  ]
  edge [
    source 17
    target 9
    label "30"
  ]
  edge [
    source 17
    target 54
    label "31"
  ]
  edge [
    source 19
    target 59
    label "26"
  ]
  edge [
    source 19
    target 44
    label "12"
  ]
  edge [
    source 19
    target 22
    label "29"
  ]
  edge [
    source 19
    target 65
    label "30"
  ]
  edge [
    source 19
    target 58
    label "31"
  ]
  edge [
    source 20
    target 30
    label "9"
  ]
  edge [
    source 20
    target 64
    label "28"
  ]
  edge [
    source 20
    target 33
    label "29"
  ]
  edge [
    source 20
    target 47
    label "31"
  ]
  edge [
    source 21
    target 17
    label "17"
  ]
  edge [
    source 21
    target 16
    label "26"
  ]
  edge [
    source 21
    target 43
    label "28"
  ]
  edge [
    source 21
    target 56
    label "30"
  ]
  edge [
    source 21
    target 40
    label "31"
  ]
  edge [
    source 22
    target 18
    label "26"
  ]
  edge [
    source 22
    target 53
    label "12"
  ]
  edge [
    source 22
    target 31
    label "30"
  ]
  edge [
    source 22
    target 10
    label "31"
  ]
  edge [
    source 23
    target 20
    label "17"
  ]
  edge [
    source 24
    target 58
    label "17"
  ]
  edge [
    source 25
    target 20
    label "17"
  ]
  edge [
    source 25
    target 23
    label "22"
  ]
  edge [
    source 25
    target 26
    label "9"
  ]
  edge [
    source 25
    target 37
    label "28"
  ]
  edge [
    source 25
    target 1
    label "31"
  ]
  edge [
    source 26
    target 30
    label "17"
  ]
  edge [
    source 27
    target 7
    label "17"
  ]
  edge [
    source 28
    target 65
    label "17"
  ]
  edge [
    source 28
    target 34
    label "22"
  ]
  edge [
    source 28
    target 37
    label "26"
  ]
  edge [
    source 28
    target 46
    label "12"
  ]
  edge [
    source 28
    target 49
    label "31"
  ]
  edge [
    source 29
    target 36
    label "17"
  ]
  edge [
    source 32
    target 60
    label "17"
  ]
  edge [
    source 34
    target 65
    label "17"
  ]
  edge [
    source 35
    target 12
    label "17"
  ]
  edge [
    source 37
    target 64
    label "17"
  ]
  edge [
    source 39
    target 58
    label "17"
  ]
  edge [
    source 39
    target 24
    label "22"
  ]
  edge [
    source 39
    target 55
    label "26"
  ]
  edge [
    source 39
    target 63
    label "12"
  ]
  edge [
    source 39
    target 49
    label "30"
  ]
  edge [
    source 40
    target 54
    label "17"
  ]
  edge [
    source 40
    target 15
    label "26"
  ]
  edge [
    source 40
    target 24
    label "28"
  ]
  edge [
    source 40
    target 35
    label "30"
  ]
  edge [
    source 41
    target 52
    label "17"
  ]
  edge [
    source 41
    target 67
    label "22"
  ]
  edge [
    source 41
    target 32
    label "28"
  ]
  edge [
    source 41
    target 26
    label "30"
  ]
  edge [
    source 41
    target 27
    label "31"
  ]
  edge [
    source 43
    target 19
    label "17"
  ]
  edge [
    source 43
    target 70
    label "26"
  ]
  edge [
    source 43
    target 66
    label "12"
  ]
  edge [
    source 43
    target 34
    label "30"
  ]
  edge [
    source 43
    target 24
    label "31"
  ]
  edge [
    source 44
    target 36
    label "26"
  ]
  edge [
    source 44
    target 53
    label "29"
  ]
  edge [
    source 44
    target 38
    label "30"
  ]
  edge [
    source 44
    target 50
    label "31"
  ]
  edge [
    source 45
    target 57
    label "26"
  ]
  edge [
    source 45
    target 10
    label "28"
  ]
  edge [
    source 45
    target 0
    label "30"
  ]
  edge [
    source 46
    target 38
    label "17"
  ]
  edge [
    source 48
    target 54
    label "17"
  ]
  edge [
    source 48
    target 40
    label "22"
  ]
  edge [
    source 48
    target 71
    label "26"
  ]
  edge [
    source 48
    target 39
    label "28"
  ]
  edge [
    source 48
    target 6
    label "30"
  ]
  edge [
    source 49
    target 62
    label "17"
  ]
  edge [
    source 51
    target 11
    label "26"
  ]
  edge [
    source 51
    target 22
    label "28"
  ]
  edge [
    source 51
    target 61
    label "30"
  ]
  edge [
    source 51
    target 45
    label "31"
  ]
  edge [
    source 52
    target 60
    label "28"
  ]
  edge [
    source 52
    target 42
    label "29"
  ]
  edge [
    source 52
    target 30
    label "30"
  ]
  edge [
    source 52
    target 7
    label "31"
  ]
  edge [
    source 54
    target 4
    label "26"
  ]
  edge [
    source 54
    target 58
    label "28"
  ]
  edge [
    source 54
    target 45
    label "29"
  ]
  edge [
    source 54
    target 12
    label "30"
  ]
  edge [
    source 55
    target 8
    label "17"
  ]
  edge [
    source 56
    target 9
    label "17"
  ]
  edge [
    source 56
    target 23
    label "26"
  ]
  edge [
    source 56
    target 34
    label "28"
  ]
  edge [
    source 56
    target 35
    label "31"
  ]
  edge [
    source 58
    target 8
    label "26"
  ]
  edge [
    source 58
    target 50
    label "12"
  ]
  edge [
    source 58
    target 10
    label "29"
  ]
  edge [
    source 58
    target 62
    label "30"
  ]
  edge [
    source 59
    target 60
    label "9"
  ]
  edge [
    source 59
    target 36
    label "12"
  ]
  edge [
    source 59
    target 18
    label "29"
  ]
  edge [
    source 59
    target 64
    label "30"
  ]
  edge [
    source 59
    target 8
    label "31"
  ]
  edge [
    source 61
    target 33
    label "26"
  ]
  edge [
    source 61
    target 31
    label "28"
  ]
  edge [
    source 61
    target 0
    label "31"
  ]
  edge [
    source 63
    target 50
    label "17"
  ]
  edge [
    source 65
    target 64
    label "26"
  ]
  edge [
    source 65
    target 38
    label "12"
  ]
  edge [
    source 65
    target 31
    label "29"
  ]
  edge [
    source 65
    target 62
    label "31"
  ]
  edge [
    source 66
    target 44
    label "17"
  ]
  edge [
    source 67
    target 52
    label "17"
  ]
  edge [
    source 68
    target 59
    label "17"
  ]
  edge [
    source 68
    target 70
    label "22"
  ]
  edge [
    source 68
    target 32
    label "9"
  ]
  edge [
    source 68
    target 29
    label "12"
  ]
  edge [
    source 68
    target 37
    label "30"
  ]
  edge [
    source 68
    target 55
    label "31"
  ]
  edge [
    source 69
    target 44
    label "17"
  ]
  edge [
    source 69
    target 66
    label "22"
  ]
  edge [
    source 69
    target 29
    label "26"
  ]
  edge [
    source 69
    target 46
    label "30"
  ]
  edge [
    source 69
    target 63
    label "31"
  ]
  edge [
    source 70
    target 59
    label "17"
  ]
  edge [
    source 71
    target 4
    label "17"
  ]
  edge [
    source 71
    target 15
    label "22"
  ]
  edge [
    source 71
    target 27
    label "9"
  ]
  edge [
    source 71
    target 55
    label "28"
  ]
  edge [
    source 71
    target 1
    label "30"
  ]
]
