#pragma once

#include <set>
#include <vector>
#include <limits.h>

class Knapsack
{
    public:
        Knapsack():
            m_cap_limits { INT_MIN },
            m_optimal_found { false }
        {}

        Knapsack(std::vector<int> caps):
            m_cap_limits { caps },
            m_curr_caps ( caps.size(), 0 ),
            m_value { 0 },
            m_optimal_found { false }
        {}

        // Cap Limit getter/settter
        std::vector<int> cap_limits() const { return m_cap_limits; }
        void cap_limits(std::vector<int> caps) { m_cap_limits = caps; }

        // Checks how much of the knapsack we have filled
        std::vector<int> curr_cap_status() { return m_curr_caps; }

        // Value getter/setter
        double value() const { return m_value; }
        void value(double val) { m_value = val; }

        // Retrieve the nodes in the knapsack
        std::set<int> node_ids() const { return m_node_ids; }

        void add_item(int i) { m_node_ids.insert(i); }

        void print();
        void solve(std::vector<int> caps, std::vector<double> vals, int n);
        
        // Optimal Flag getter/setter
        bool op() const { return m_optimal_found; }
        void op(bool found) { m_optimal_found = found; }
        
        // Remove values, optimal flag, and nodes from the Knapsack
        void reset();

        // Used for LR: rotate the capacities from left to right
        void rotate_caps();

    private:
        std::vector<int> m_cap_limits;
        std::vector<int> m_curr_caps;

        double m_value;
        std::set<int> m_node_ids;
        std::vector<std::vector<int>> m_value_table;
        bool m_optimal_found;
};
