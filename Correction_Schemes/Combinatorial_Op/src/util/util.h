#pragma once

#include <vector>
#include <iostream>
#include <set>
#include <deque>

int knap_max(int a, int b);
void vecMult(std::vector<int> &v, double k);
void vecDiv(std::vector<int> &v, double k);
void vecAdd(std::vector<int> &a, std::vector<int> b);

template < class T >
std::ostream& operator << (std::ostream& os, const std::vector<T>& v) 
{
    os << "[";
    for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << " ]";
    return os;
};

template < class T >
std::ostream& operator << (std::ostream& os, const std::set<T>& v) 
{
    os << "[";
    for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << " ]";
    return os;
};

template < class T >
std::ostream& operator << (std::ostream& os, const std::deque<T>& v) 
{
    os << "[";
    for (typename std::deque<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << " ]";
    return os;
};