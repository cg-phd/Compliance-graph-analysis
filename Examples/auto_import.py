#!/usr/bin/python3

import networkx as nx
import igraph as ig
import graphviz

def get_network(dotfile, f):
  #A = nx.drawing.nx_agraph.to_agraph(nx.drawing.nx_pydot.read_dot(dotfile))
  #A.layout('dot')
  #if (A.has_node('\\n')):
  #  A.remove_node('\\n') # Remove "newline" node from newline end of dot file
  #G=nx.DiGraph(A)
  
  G = nx.drawing.nx_agraph.read_dot(dotfile)
  #A = nx.drawing.nx_agraph.to_agraph(nx.drawing.nx_pydot.read_dot(dotfile))
  #A.layout('dot')
  #A.draw('tree.png')
  if(G.has_node('\\n')):
    G.remove_node('\\n') # Remove "newline" node from newline end of dot file
  
  network = ig.Graph.from_networkx(G)
  network.write_graphml(f)

  return network  
