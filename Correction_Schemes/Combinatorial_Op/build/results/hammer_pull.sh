#!/bin/bash

#scp nschrick@hammer:~/Diss/Corr_Schemes/Combinatorial_Op/build/car.csv .
#scp nschrick@hammer:~/Diss/Corr_Schemes/Combinatorial_Op/build/hipaa.csv .
#scp nschrick@hammer:~/Diss/Corr_Schemes/Combinatorial_Op/build/osha.csv .

scp nschrick@hammer:~/Diss/Corr_Schemes/Combinatorial_Op/build/*.csv /home/noah/Documents/School/Dissertation/Dissertation/Analysis/Correction_Schemes/Combinatorial_Op/build/results
