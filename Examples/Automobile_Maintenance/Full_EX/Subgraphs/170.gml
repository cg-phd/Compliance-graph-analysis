graph [
  directed 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "911"
  ]
  node [
    id 1
    label "910"
  ]
  node [
    id 2
    label "921"
  ]
  node [
    id 3
    label "906"
  ]
  node [
    id 4
    label "916"
  ]
  node [
    id 5
    label "925"
  ]
  node [
    id 6
    label "920"
  ]
  node [
    id 7
    label "914"
  ]
  node [
    id 8
    label "922"
  ]
  node [
    id 9
    label "908"
  ]
  node [
    id 10
    label "918"
  ]
  node [
    id 11
    label "913"
  ]
  node [
    id 12
    label "912"
  ]
  node [
    id 13
    label "917"
  ]
  node [
    id 14
    label "923"
  ]
  node [
    id 15
    label "907"
  ]
  node [
    id 16
    label "919"
  ]
  node [
    id 17
    label "924"
  ]
  node [
    id 18
    label "905"
  ]
  node [
    id 19
    label "915"
  ]
  node [
    id 20
    label "909"
  ]
  edge [
    source 0
    target 19
    label "21"
  ]
  edge [
    source 0
    target 16
    label "20"
  ]
  edge [
    source 1
    target 5
    label "21"
  ]
  edge [
    source 6
    target 8
    label "20"
  ]
  edge [
    source 7
    target 19
    label "21"
  ]
  edge [
    source 7
    target 17
    label "20"
  ]
  edge [
    source 8
    target 5
    label "21"
  ]
  edge [
    source 9
    target 1
    label "20"
  ]
  edge [
    source 11
    target 19
    label "21"
  ]
  edge [
    source 12
    target 19
    label "21"
  ]
  edge [
    source 12
    target 8
    label "20"
  ]
  edge [
    source 13
    target 16
    label "20"
  ]
  edge [
    source 14
    target 17
    label "20"
  ]
  edge [
    source 16
    target 5
    label "21"
  ]
  edge [
    source 17
    target 5
    label "21"
  ]
  edge [
    source 18
    target 1
    label "20"
  ]
  edge [
    source 18
    target 19
    label "21"
  ]
  edge [
    source 19
    target 5
    label "20"
  ]
]
