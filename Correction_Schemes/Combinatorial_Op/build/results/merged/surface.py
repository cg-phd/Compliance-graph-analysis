#import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import pandas as pd
#from scipy import interpolate, stats

def plot_surfs(df, title, type):
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

    m = df['Monetary'].to_numpy()
    t = df['Time'].to_numpy()
    if (type == "dg"):
        dg = df['DG'].to_numpy()
    elif (type == "roim"):
        dg = df['ROIM'].to_numpy()
    else:
        dg = df['ROIT'].to_numpy()

    if ("Automobile" in title):
        # Need to range time from 0->200
        mon_unit = "[$ (per Year)]"
        time_unit = "[Hours (per Year)]"
        max_mon_z = 200
        min_mon_z = -100
        pr = "Automobile Maintenance"
    elif("HIPAA" in title):
        # Need to range time from 0->350
        mon_unit = "[$ (x100 per Year)]"
        time_unit = "[Hours (x10 per Year)]"
        max_mon_z = 1500
        min_mon_z = -300
        pr = "HIPAA"
    elif("OSHA" in title):
        mon_unit = "[$ (x100 per Year)]"
        time_unit = "[Hours (per Year)]"
        max_mon_z = 2500
        min_mon_z = -300
        pr = "OSHA 1910H"
    else:
        print("Error, title not recognized.")
        return

    m = m[::10]
    t = t[::10]
    dg = dg[::10]

    max_tz = 50
    min_tz = -200

    max_dg = dg.max()
    min_dg = dg.min()
    mean_dg = dg.mean()
    std_dg = dg.std()
    print(pr + " max " + type + " = ", max_dg)
    print(pr + " min " + type + " = ", min_dg)
    print(pr + " mean " + type + " = ", mean_dg)
    print(pr + " std dev " + type + " = ", std_dg)


    trisurf = ax.plot_trisurf(m, t, dg,
                cmap=cm.cool,
                linewidth = 0.2,
                edgecolor = 'grey')
    
    fig.colorbar(trisurf, ax = ax, shrink = 0.5, aspect = 5)
    ax.set_xlabel('Allocated Monetary Budget \n' + mon_unit, fontweight ='bold', labelpad=10) 
    ax.set_ylabel('Allocated Time Budget\n' + time_unit, fontweight ='bold', labelpad=10) 


    if (type.lower() == "dg"):
        ax.set_zlabel('Duality Gap', fontweight ='bold')
        ax.set_zlim([0,1])
        ax.set_title("Duality Gap Across Constraint Ranges\n for the " + title + " Network")
    elif (type.lower() == "roim"):
        ax.set_zlabel('Monetary ROI (%)', fontweight ='bold')
        ax.set_zlim([min_mon_z, max_mon_z])
        ax.set_title("Monetary Return on Investment Across Constraint Ranges\n for the " + title + " Network")
    elif (type.lower() == "roit"):
        ax.set_zlabel('Time ROI (%)', fontweight ='bold')
        ax.set_zlim([min_tz, max_tz])
        ax.set_title("Time Return on Investment Across Constraint Ranges\n for the " + title + " Network")
    else:
        print("Error, unknown 'type' parameter plot")

    plt.show()


def main():
    indiv_plots = 0
    plot_rois = 1
    plot_dg = 1

    plot_car = 1
    plot_hip = 1
    plot_osha = 1

    if (not indiv_plots and (not plot_car or not plot_hip or not plot_osha)):
        print("Error: Can't create grouped plots without plotting all networks.")
        return
    ############
    ### CAR
    ############
    if (plot_car):
        car_df = pd.read_csv("final_car.csv")
        car_df['ROIM'] = car_df['ROIM'].apply(lambda x: x*100)
        car_df['ROIT'] = car_df['ROIT'].apply(lambda x: x*100)

        car_df = car_df.astype({'Monetary': 'int16', 'Time': 'int16', 'DG' : 'float32', 'ROIM' : 'float32', 'ROIT' : 'float32'})
        bad_test = car_df[car_df['DG']<0]
        car_df = car_df.drop(bad_test.index)
        other_bad_test = car_df[car_df['DG']>1]
        car_df = car_df.drop(other_bad_test.index)
        filter_unreas = car_df[car_df['Time'] > 200]
        car_df = car_df.drop(filter_unreas.index)

        car_mean = car_df['DG'].mean()

        car_df = car_df[car_df['DG'] < 5*car_mean]
        if (indiv_plots):
            boxplot = car_df['DG'].plot(kind='box', title='Duality Gap Boxplot for the Automobile Maintenance Network')  
            plt.show()
            boxplot = car_df['ROIM'].plot(kind='box', title='Monetary ROI (%) Boxplot for the Automobile Maintenance Network')  
            plt.show()
            boxplot = car_df['ROIT'].plot(kind='box', title='Time ROI (%) Boxplot for the Automobile Maintenance Network')  
            plt.show()
        
        if (plot_dg):
            plot_surfs(car_df, "Automobile Maintenance", "dg")
        if (plot_rois):
            plot_surfs(car_df, "Automobile Maintenance", "roim")
            plot_surfs(car_df, "Automobile Maintenance", "roit")

    ############
    ### HIPAA
    ############
    if (plot_hip):
        hip_df = pd.read_csv("final_hipaa.csv")
        hip_df = hip_df.astype({'Monetary': 'int16', 'Time': 'int16', 'DG' : 'float32', 'ROIM' : 'float32', 'ROIT' : 'float32'})
        hip_df['ROIM'] = hip_df['ROIM'].apply(lambda x: x*100)
        hip_df['ROIT'] = hip_df['ROIT'].apply(lambda x: x*100)

        bad_test = hip_df[hip_df['DG']<0]
        hip_df = hip_df.drop(bad_test.index)
        other_bad_test = hip_df[hip_df['DG']>1]
        hip_df = hip_df.drop(other_bad_test.index)
        hip_mean = hip_df['DG'].mean()
        hip_df = hip_df[hip_df['DG'] < 5*hip_mean]

        if (indiv_plots):
            boxplot = hip_df['DG'].plot(kind='box', title='Duality Gap Boxplot for the HIPAA Network')  
            plt.show()
            boxplot = hip_df['ROIM'].plot(kind='box', title='Monetary ROI (%) Boxplot for the HIPAA Network')  
            plt.show()
            boxplot = hip_df['ROIT'].plot(kind='box', title='Time ROI (%) Boxplot for the HIPAA Network')  
            plt.show()
        
        if (plot_dg):
            plot_surfs(hip_df, "HIPAA", "dg")
        if (plot_rois):
            plot_surfs(hip_df, "HIPAA", "roim")
            plot_surfs(hip_df, "HIPAA", "roit")

    ############
    ### OSHA
    ############    
    if (plot_osha):
        osha_df = pd.read_csv("final_osha.csv")
        osha_df = osha_df.astype({'Monetary': 'int16', 'Time': 'int16', 'DG' : 'float32', 'ROIM' : 'float32', 'ROIT' : 'float32'})
        osha_df['ROIM'] = osha_df['ROIM'].apply(lambda x: x*100)
        osha_df['ROIT'] = osha_df['ROIT'].apply(lambda x: x*100)

        bad_test = osha_df[osha_df['DG']<0]
        osha_df = osha_df.drop(bad_test.index)
        other_bad_test = osha_df[osha_df['DG']>1]
        osha_df = osha_df.drop(other_bad_test.index)
        filter_unreas = osha_df[osha_df['Time'] > 300]
        osha_df = osha_df.drop(filter_unreas.index)
        osha_mean = osha_df['DG'].mean()
        osha_df = osha_df[osha_df['DG'] < (5*osha_mean)]

        if (indiv_plots):
            boxplot = osha_df['DG'].plot(kind='box', title='Duality Gap Boxplot for the OSHA 1910H Network')  
            plt.show()
            boxplot = osha_df['ROIM'].plot(kind='box', title='Monetary ROI Boxplot for the OSHA 1910H Network')  
            plt.show()
            boxplot = osha_df['ROIT'].plot(kind='box', title='Time ROI Boxplot for the OSHA 1910H Network')  
            plt.show()
        if (plot_dg):    
            plot_surfs(osha_df, "OSHA 1910H", "dg")
        if (plot_rois):
            plot_surfs(osha_df, "OSHA 1910H", "roim")
            plot_surfs(osha_df, "OSHA 1910H", "roit")

    ############
    ### GROUPED PLOTS
    ############      
    if(not indiv_plots):
        combined_dfs = pd.DataFrame({'Automobile Maintenance': car_df['DG'],
                             'HIPAA': hip_df['DG'],
                             'OSHA 1910H': osha_df['DG']})
        combined_dfs.plot(kind='box', title='Duality Gap Boxplots for Example Networks', ylim = [-0.25, 1.5])
        plt.show()
       
        ROIM_dfs = pd.DataFrame({'Automobile Maintenance': car_df['ROIM'],
                             'HIPAA': hip_df['ROIM'],
                             'OSHA 1910H': osha_df['ROIM']})
        ROIM_dfs.plot(kind='box', title='Monetary ROI (%) Boxplots for Example Networks', ylim = [-150, 3000])
        plt.show()

        ROIT_dfs = pd.DataFrame({'Automobile Maintenance': car_df['ROIT'],
                             'HIPAA': hip_df['ROIT'],
                             'OSHA 1910H': osha_df['ROIT']})
        ROIT_dfs.plot(kind='box', title='Time ROI (%) Boxplots for Example Networks', ylim = [-200, 50])
        plt.show()
           
if __name__ == "__main__":
    main()