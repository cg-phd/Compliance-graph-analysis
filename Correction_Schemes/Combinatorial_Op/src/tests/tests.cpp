#include <cmath>
#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <iterator>

#include "../pkn/pkn.h"
#include "../mdkp/Relaxation.h"
#include "../util/util.h"

// Confirming that constructors, getters, and setters work as intended
int object_creation_test()
{
    std::vector mo_cost = {5, 1};
    std::vector mt_cost = {10, 5};

    Mitigation mfirst(0, mo_cost, false);
    Mitigation msec(1, mt_cost, false);

    Exploit etest(0);
    etest.add_mit(mfirst);
    etest.add_mit(msec);

    assert(etest.mits().size() == 2 && "Obtained Number of Mitigations incorrect!");

    assert(etest.mits()[0].costs() == mo_cost && "Obtained Mitigation 0 Costs incorrect!");
    assert(etest.mits()[0].id() == 0 && "Obtained Mitigation 0 ID incorrect!");
    assert(etest.mits()[0].is_global_mit() == false && "Obtained Mitigation 0 Global incorrect!");

    assert(etest.mits()[1].costs() == mt_cost && "Obtained Mitigation 1 Costs incorrect!");
    assert(etest.mits()[1].id() == 1 && "Obtained Mitigation 1 ID incorrect!");
    assert(etest.mits()[1].is_global_mit() == false && "Obtained Mitigation 1 Global incorrect!");

    Node ntest(0, 10.0, etest);
    ntest.print();

    assert(ntest.id() == 0 && "Obtained Node ID incorrect!");
    assert(ntest.severity() == 10 && "Obtained Node Severity incorrect!");
    assert((int) ntest.exploits().size() == 1 && "Obtained Node number of exploits incorrect!");

    return 0;
}

// Confirming that the basic knapsack problem with 1-dimensional weights works as intended
int base_knapsack_test()
{
    /* Base Test */
    std::cout << "/* Base Test */" << std::endl;
    std::vector<int> cap_vect = { 50, 20 };
    Knapsack my_knap(cap_vect);
    std::vector<double> base_val = { 60, 100, 120 };
    std::vector<int> wt = { 10, 20, 30 };
    int n = base_val.size();
    my_knap.solve(wt, base_val, n);
    my_knap.print();
    std::cout << std::endl;

    assert(my_knap.value() == 220 && "Obtained Knapsack value is incorrect!");

    return 0;
}

/* Two confirmations:
    1. That performing Langragian Relaxation ONCE (a single time with a given lambda) produces the correct upper bound and mitigated node list
    2. That optimizing the LR knapsack (minimizing lambda) by repeatedly performing LR produces the correct upper bound, real knapsack value, mitigated node list, and duality gap
*/
int lr_test()
{
    /* LR Test */
    std::cout << "/* LR Test */" << std::endl;
    std::vector<double> lr_val = { 2, 3, 5, 10, 2, 10, 10, 13, 4, 8, 6, 4 };
    std::vector<int> lr_caps = { 15, 12 };

    std::deque<std::vector<int>> constraints;
    std::vector<int> cost = { 6, 10, 6, 2, 10, 5, 2, 6, 3, 5, 5, 10 };
    std::vector<int> time = { 2, 3, 8, 6, 2, 6, 4, 4, 4, 2, 5, 7 };
    std::vector<double> lambdas = { 0 };

    constraints.push_back(time);
    int num_items = lr_val.size();

    std::vector<double> adjusted = LR(lr_val, constraints, lambdas); 
    
    Knapsack lr_sack(lr_caps);
    lr_sack.solve(cost, LR(lr_val, constraints, lambdas), num_items);
    lr_sack.value(lr_sack.value() + std::round(lambdas[0]*lr_caps[1]));
    lr_sack.print();
    assert(lr_sack.value() == 43 && "Obtained upper bound value is incorrect!");
    std::set<int> known_lr_s = {3,5,6,7};
    assert(lr_sack.node_ids() == known_lr_s && "Obtained mitigated nodes is incorrect!");


    std::cout << std::endl;

    std::cout << "/* LR OP Test */" << std::endl;
    constraints.emplace_front(cost);
    Knapsack op_lr_sack(lr_caps);
    LR_solve(op_lr_sack, lr_val, constraints, lambdas);

    std::cout << "Obtained Upper Bound: " << op_lr_sack.value() << std::endl;
    assert(op_lr_sack.value() == 33 && "Obtained upper bound value is incorrect!");

    std::cout << "Mitigated Nodes in LR Knapsack: " << op_lr_sack.node_ids() << std::endl;
    std::set<int> known_s = {6,7,9};
    assert(op_lr_sack.node_ids() == known_s && "Obtained mitigated nodes is incorrect!");

    std::set<int> lr_nodes = op_lr_sack.node_ids();
    int real_val = 0;
    for (int n : lr_nodes)
        real_val += lr_val[n];

    std::cout << "Real Knapsack Value: " << real_val << std::endl;
    assert(real_val == 31 && "Obtained real value is incorrect!");

    float power_of_10 = std::pow(10, 7);
    double dual_gap = static_cast<double>((op_lr_sack.value() - real_val)/static_cast<double>(real_val));
    dual_gap = std::round(dual_gap * power_of_10) / power_of_10;

    std::cout << "Duality Gap: " << dual_gap << std::endl;
    assert(dual_gap == 0.0645161 && "Obtained duality gap is incorrect!");

    std::cout << std::endl;

    return 0;
}

// Confirming that the manual construction of the prior-knowledge network matches the auto parse and construction of the problem space
int ps_test()
{
    /* Known Values */
    Mitigation mfirst(0, std::vector<int> {80, 242}, false);

    Exploit ezero(0);
    ezero.add_mit(mfirst);
    ezero.name("brake_pads");

    Exploit eone(1);
    eone.add_mit(mfirst);
    eone.name("exhaust_pipes");

    Exploit etwo(2);
    etwo.add_mit(mfirst);
    etwo.name("ac_filter");

    Exploit ethree(3);
    ethree.add_mit(mfirst);
    ethree.name("vaccuum_pump");

    Exploit efour(4);
    efour.add_mit(mfirst);
    efour.name("brake_service");

    Exploit efive(5);
    efive.add_mit(mfirst);
    efive.name("time_advance");
    
    //Node nzero(0, 16.3, eone); // Reminder: node 0 does NOT have any in_edges. There should be no affiliated exploit.
    Node nzero(0, 16.3);

    Node none(1, 14.2, ezero);
    none.add_exploit(eone);
    none.add_exploit(etwo);

    Node ntwo(2, 12.1, ethree);
    ntwo.add_exploit(efive);

    Node nthree(3, 10, ezero);
    nthree.add_exploit(eone);
    nthree.add_exploit(etwo);

    Node nfour(4, 8.9, eone);
    nfour.add_exploit(efour);
    nfour.add_exploit(efive);

    /* Problem Space */
    std::cout << "/* Problem Space Test */" << std::endl;
    ProblemSpace test_space;
    std::string value_file = "../src/tests/prio.txt";
    std::string e_file = "../src/tests/known_e.txt";
    std::string pkn_file = "../src/tests/pkn.ini";

    test_space.prepare(value_file, e_file, pkn_file, 1, "year");
    std::cout << "Test Space: " << std::endl;
    test_space.print();

    // Number of Nodes
    assert((int) test_space.nodes().size() == 5 && "Number of nodes incorrect!");

    // Granular check for debugging before delving straight into recursive operator== overload
    // We know there is only one exploit and one mitigation at Node 0
    //for (Exploit e : test_space.nodes()[0].exploits()){
    for (Exploit e : (*(test_space.nodes().begin())).exploits())
    {
        std::cout << "Known Mitigation 0 for Exploit 1:" << std::endl;
        mfirst.print();
        std::cout << "Generated Mitigation 0 for Exploit 1:" << std::endl;
        e.mits()[0].print();

        assert(e.mits()[0] == mfirst && "Node 0 Mitigations Incorrect!");
        std::cout << std::endl;

        std::cout << "Known Exploit 1:" << std::endl;
        eone.print();
        std::cout << "Generated Exploit 1:" << std::endl;
        e.print();

        assert(e == eone && "Node 0 Exploits Incorrect!");
    }

    std::vector<Node>  nodelist = {nzero, none, ntwo, nthree, nfour};
    int idx = 0;
    for (Node n : test_space.nodes())
    {
        std::cout << "Node " << idx << std::endl;
        assert(n == nodelist[idx] && "Node incorrect");
        idx++;
    }

    return 0;
}

// Confirming that the Lagrangian Relaxation performed on Mitigations successfully reduces the set of Mitigations to 1
int mitigation_reduction_test()
{
    /* Testing with all lambdas = 0, only 2 costs */
    std::cout << "*** Testing with all lambdas = 0, only 2 costs *** " << std::endl;

    std::vector<double> zero_lam = {0};
    int base_val = 10;

    Mitigation zero(0, {10, 5}, false);
    double test_lr = zero.lr(base_val, zero_lam);
    assert(test_lr == 0 && "Invalid LR value when reducing Mitigation 0");

    Mitigation one(1, {1, 3}, false);
    test_lr = one.lr(base_val, zero_lam);
    assert(test_lr == 9 && "Invalid LR value when reducing Mitigation 1");

    Mitigation two(2, {5, 7}, true);
    test_lr = two.lr(base_val, zero_lam);
    assert(test_lr == 5 && "Invalid LR value when reducing Mitigation 2");

    Exploit ez(0, "test exploit", true, {zero, one, two});

    ez.reduce_mits(base_val, zero_lam);
    assert(ez.mits()[0].id() == -1 && "Reducing to a single mitigation failed for Exploit 0: no -1 ID Mitigation found");
    assert(ez.mits()[0].costs() == one.costs() && "Reducing to a single mitigation failed for Exploit 0: Mitigation ID -1 does not have the correct costs");
    assert(ez.mits()[0].is_global_mit() == one.is_global_mit() && "Reducing to a single mitigation failed for Exploit 0: Mitigation ID -1 does not have the correct global flag");

    ez.print();
    std::cout << std::endl;

    /* Testing with all lambdas = 1.75, only 2 costs */
    std::cout << "*** Testing with all lambdas = 1.75, only 2 costs *** " << std::endl;

    std::vector<double> osf_lam = {1.75};

    Mitigation three(0, {10, 5}, false);
    test_lr = three.lr(base_val, osf_lam);
    assert(std::abs(test_lr - -8.75) < 0.0001 && "Invalid LR value when reducing Mitigation 3");

    Mitigation four(1, {1, 3}, false);
    test_lr = four.lr(base_val, osf_lam);
    assert(std::abs(test_lr - 3.75) < 0.0001 && "Invalid LR value when reducing Mitigation 4");

    Mitigation five(2, {5, 7}, true);
    test_lr = five.lr(base_val, osf_lam);
    assert(std::abs(test_lr - -7.25) < 0.0001 && "Invalid LR value when reducing Mitigation 5");

    Exploit eo(1, "test exploit", true, {three, four, five});

    eo.reduce_mits(base_val, osf_lam);
    assert(eo.mits()[0].id() == -1 && "Reducing to a single mitigation failed for Exploit 1 : no -1 ID Mitigation found");
    assert(eo.mits()[0].costs() == four.costs() && "Reducing to a single mitigation failed for Exploit 1: Mitigation ID -1 does not have the correct costs");
    assert(eo.mits()[0].is_global_mit() == four.is_global_mit() && "Reducing to a single mitigation failed for Exploit 1: Mitigation ID -1 does not have the correct global flag");

    eo.print();
    std::cout << std::endl;

    /* Testing with all lambdas = 0.667, only 2 costs */
    std::cout << "*** Testing with all lambdas = 0.667, only 2 costs *** " << std::endl;

    std::vector<double> s_lam = {0.667};

    Mitigation six(0, {10, 5}, false);
    test_lr = six.lr(base_val, s_lam);
    assert(std::abs(test_lr - -3.335) < 0.0001  && "Invalid LR value when reducing Mitigation 6");

    Mitigation seven(1, {1, 3}, false);
    test_lr = seven.lr(base_val, s_lam);
    assert(std::abs(test_lr - 6.999) < 0.0001 && "Invalid LR value when reducing Mitigation 7");

    Mitigation eight(2, {5, 7}, true);
    test_lr = eight.lr(base_val, s_lam);
    assert(std::abs(test_lr - 0.331) < 0.0001 && "Invalid LR value when reducing Mitigation 8");

    Exploit et(1, "test exploit", true, {six, seven, eight});

    et.reduce_mits(base_val, s_lam);
    assert(et.mits()[0].id() == -1 && "Reducing to a single mitigation failed for Exploit 2 : no -1 ID Mitigation found");
    assert(et.mits()[0].costs() == seven.costs() && "Reducing to a single mitigation failed for Exploit 2: Mitigation ID -1 does not have the correct costs");
    assert(et.mits()[0].is_global_mit() == seven.is_global_mit() && "Reducing to a single mitigation failed for Exploit 2: Mitigation ID -1 does not have the correct global flag");

    et.print();
    std::cout << std::endl;

    /* Testing with lambdas = 2.34, 7.89, => 3 costs */
    std::cout << "*** Testing with with lambdas = 2.34, 7.89 => 3 costs *** " << std::endl;
       
    std::vector<double> two_lam = {2.34, 7.89};

    Mitigation nine(0, {10, 5, 9}, false);
    test_lr = nine.lr(base_val, two_lam);
    assert(std::abs(test_lr - -82.71) < 0.0001  && "Invalid LR value when reducing Mitigation 9");

    Mitigation ten(1, {1, 3, 6}, false);
    test_lr = ten.lr(base_val, two_lam);
    assert(std::abs(test_lr - -45.36) < 0.0001 && "Invalid LR value when reducing Mitigation 10");

    Mitigation eleven(2, {5, 7, 13}, true);
    test_lr = eleven.lr(base_val, two_lam);
    assert(std::abs(test_lr - -113.95) < 0.0001 && "Invalid LR value when reducing Mitigation 11");

    Exploit eth(3, "test exploit", true, {nine, ten, eleven});

    eth.reduce_mits(base_val, two_lam);
    assert(eth.mits()[0].id() == -1 && "Reducing to a single mitigation failed for Exploit 3 : no -1 ID Mitigation found");
    assert(eth.mits()[0].costs() == ten.costs() && "Reducing to a single mitigation failed for Exploit 3: Mitigation ID -1 does not have the correct costs");
    assert(eth.mits()[0].is_global_mit() == ten.is_global_mit() && "Reducing to a single mitigation failed for Exploit 3: Mitigation ID -1 does not have the correct global flag");

    eth.print();
    std::cout << std::endl;


    /* Testing with lambdas = 2.34, 7.89, 0.13, 3.9,  => 5 costs */
    std::cout << "*** Testing with with lambdas = 2.34, 7.89, 0.13, 3.9 => 5 costs *** " << std::endl;
       
    std::vector<double> four_lam = {2.34, 7.89, 0.13, 3.9};

    Mitigation twelve(0, {10, 5, 9, 17, 12}, false);
    test_lr = twelve.lr(base_val, four_lam);
    assert(std::abs(test_lr - -131.72) < 0.0001  && "Invalid LR value when reducing Mitigation 9");

    Mitigation thirteen(1, {1, 3, 6, 9, 11}, false);
    test_lr = thirteen.lr(base_val, four_lam);
    assert(std::abs(test_lr - -89.43) < 0.0001 && "Invalid LR value when reducing Mitigation 10");

    Mitigation fourteen(2, {5, 7, 13, 21, 15}, true);
    test_lr = fourteen.lr(base_val, four_lam);
    assert(std::abs(test_lr - -175.18) < 0.0001 && "Invalid LR value when reducing Mitigation 11");

    Exploit ef(4, "test exploit", true, {twelve, thirteen, fourteen});

    ef.reduce_mits(base_val, four_lam);
    ef.reduce_mits(base_val, four_lam); // Call twice to confirm that the existing Reduced Mitigation (-1 ID) is removed and not duplicated
    assert(ef.mits()[0].id() == -1 && "Reducing to a single mitigation failed for Exploit 4 : no -1 ID Mitigation found");
    assert(ef.mits()[0].id() == -1 && ef.mits()[1].id() != -1 && "Reducing to a single mitigation failed for Exploit 4 : duplicated -1 ID Mitigation found");
    assert(ef.mits()[0].costs() == thirteen.costs() && "Reducing to a single mitigation failed for Exploit 4: Mitigation ID -1 does not have the correct costs");
    assert(ef.mits()[0].is_global_mit() == ten.is_global_mit() && "Reducing to a single mitigation failed for Exploit 4: Mitigation ID -1 does not have the correct global flag");

    ef.print();
    std::cout << std::endl;

    return 0;
}

// Confirming that a set of Exploits are successfully collapsed into a single Exploit
// Does NOT confirm if mitigation reduction is working as intended - see mitigation_reduction_test() for that confirmation.
int exploit_collapse_test()
{
    // Create Mitigations
    Mitigation mzero(0, {5,10, 3}, false);
    Mitigation mone(0, {15, 7}, true);
    Mitigation mtwo(0, {21,5,10,1,93}, false);

    // Create Exploits
    Exploit ezero(0, "Test Exploit 0", true, {mzero});
    Exploit eone(1, "Test Exploit 1", true, {mone});
    Exploit etwo(2, "Test Exploit 2", true, {mtwo});

    // Create Nodes
    Node nzero(0, 10);
    nzero.exploits({ezero});

    Node none(1, 9.7);
    none.exploits({ezero, eone});

    Node ntwo(2, 13.54);
    ntwo.exploits({ezero, eone, etwo});

    // Create ProblemSpace
    ProblemSpace test_space;
    test_space.nodes({nzero, none, ntwo});
    std::cout << "Pre Collapse:" << std::endl;
    test_space.print();

    test_space.collapse_exploits({0, 1, 2, 3});
    std::cout << "Post Collapse:" << std::endl;
    test_space.print();

    // Node 0
    //std::set<Node>::iterator it = test_space.nodes().begin();
    int idx = 0;
    std::vector<std::vector<int>> costs = {{5,10,3}, {20,17,3}, {41,22,13,1,93}};

    for (Node n : test_space.nodes())
    {
        std::cout << "Node " << idx << std::endl;
        assert((*(n.exploits().begin())).id() == -1 && "Error: No Collapsed Exploit with -1 ID Found in Node");
        assert((*(n.exploits().begin())).mits().size() == 1 && "Error: Collapsed Exploit in Node does not have a single Mitigation");
        assert((*(n.exploits().begin())).mits()[0].costs() == costs[idx] && "Error: Collapsed Exploit for Node has incorrect costs");
        assert((*(n.exploits().begin())).mits()[0].is_global_mit() == false && "Error: Incorrect Global Flag for Node 0");
        idx++;
    }

    return 0;
}

// Running the individual tests
int main()
{
    std::cout << "Running Object Creation Test." << std::endl;
    int ret = object_creation_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "Running Base Knapsack Test." << std::endl;
    ret = base_knapsack_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "Running LR Knapsack Test." << std::endl;
    ret = lr_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "Running Problem Space Test." << std::endl;
    ret = ps_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "Running Mitigation Reduction Test." << std::endl;
    ret = mitigation_reduction_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "Running Exploit Collapse Test." << std::endl;
    ret = exploit_collapse_test();
    assert(ret == 0);
    std::cout << "No Failure." << std::endl;
    std::cout << std::endl;

    std::cout << "**************** All Unit Tests Successfully Passed ****************" << std::endl;
    return 0;
}