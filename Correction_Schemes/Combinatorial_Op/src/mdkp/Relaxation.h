#pragma once

#include <vector>
#include <deque>
#include <limits>
#include <cfloat>

#include "MDKP.h"
#include "../pkn/Exploit.h"

class Relaxation
{
    public:
        // For providing a custom stop tolerance
        Relaxation(Knapsack k, double t): 
            m_ub { INT_MAX },
            m_loop_count { 0 },
            m_tol { t },
            m_stop { false },
            m_lambdas ( k.cap_limits().size()-1, 0 ),
            m_last_lambdas ( k.cap_limits().size()-1, 0 ),
            m_last_slack ( k.cap_limits().size()-1, DBL_MAX ), 
            m_last_vio ( k.cap_limits().size()-1, 0 ),
            m_sack { std::move(k) }
        {}

        Relaxation(Knapsack k):
            Relaxation(k, 1e-8) // default tolerance: 1e-8
        {}


        double tol() const { return m_tol; }
        void tol(double t) { m_tol = t; }

        std::vector<double> lambdas() const { return m_lambdas; }
        void lambdas(std::vector<double> lamb) { m_lambdas = lamb; }

        std::vector<double> last_lambdas() const { return m_last_lambdas; }


        std::set<int> best_nodes() const { return m_best_nodes; }
        void best_nodes(std::set<int> nodes) { m_best_nodes = nodes; }
        Knapsack knap() { return m_sack; }

        void iterate(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double>  antisevs);
        void test_LR_solve(std::vector<double> vals, std::deque<std::vector<int>> weights);

        bool stop() { return m_stop; }
        void stop(bool s) { m_stop = s; }

        void mitigated_exploits(Exploit e) { m_mitigated_exploits.insert(e); }
        void mitigated_exploits(std::set<Exploit> e) { m_mitigated_exploits = e; }
        std::set<Exploit> mitigated_exploits() const { return m_mitigated_exploits; }

        std::vector<double> best_lambdas() const { return m_best_lambdas; }

        int loop_count() const { return m_loop_count; }
        int ub() const { return m_ub; }

        void reset();

    private:
        double m_ub; // upper bound
        int m_loop_count; // for external analysis only
        double m_tol; // stop tolerance
        bool m_stop; // tolerance check
        std::vector<double> m_lambdas;
        std::vector<double> m_last_lambdas;
        std::vector<double> m_last_slack; // last known lambdas that were slack
        std::vector<double> m_last_vio; // last known lambdas that violated the constraints
        
        std::set<int> m_best_nodes; // keep track of nodes with highest knapsack value that is a feasible solution
        std::set<Exploit> m_mitigated_exploits; // keep track of mitigated exploits
        std::vector<double> m_best_lambdas; 
        
        Knapsack m_sack;

};

std::vector<double> LR(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambda);
std::vector<double> antiLR(std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambda);

void LR_solve(Knapsack &lr_sack, std::vector<double> vals, std::deque<std::vector<int>> weights, std::vector<double> lambdas);

