# Extract all the in-edges for each node (excluding node 0 - it's not preventable).
# Also extracts any nodes that are already flagged as in violation.

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-g", "--gdotfile", dest = "gdotfile", default = None, help = "Graphviz dotfile")
parser.add_argument("-e", "--exploit", dest = "efname", default = "known_e.txt", help = "Name of the exploit output file")
parser.add_argument("-v", "--violation", dest = "vfname", default = "vio.txt", help = "Name of the violation output file")
parser.add_argument("-d", "--directory", dest = "directory", default = ".", help = "Output directory")

args = parser.parse_args()

if args.gdotfile is None:
    print("Error: No dotfile provided.")
    exit(-1)

dotfile = open(args.gdotfile, 'r')
lines = dotfile.readlines()
dotfile.close()

edgelist = {}
vio_nodes = []

for line in lines:
    if "fillcolor=red" in line:
        vio_nodes.append(line.split(" ")[0])
    if "->" in line:
        node = line.split("->")[1].split(" [")[0]
        edge = line.split("->")[1].split("[label=")[1].strip("'];\n'")
        edgelist.setdefault(node, []).append(edge)


outfile = open((args.directory + "/" + args.efname), 'w')
for key, val in edgelist.items():
    print(*val, file=outfile)

voutfile = open((args.directory + "/" + args.vfname), 'w')
for node in vio_nodes:
    print(*node, file=voutfile)
