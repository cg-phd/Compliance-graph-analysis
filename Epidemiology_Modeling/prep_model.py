#!/usr/bin/python3 

import networkx as nx
import matplotlib.pyplot as plt
from collections import OrderedDict
from operator import getitem
import itertools, os, sys
import numpy as np
import configparser

# Change dir to location of this python file
#print(os.getcwd())
#os.chdir(os.path.dirname(sys.argv[0]))
#print(os.getcwd())

## dotfile is the Graphviz dotfile of the graph
## pkn is the prior-knowledge network ini file
## warning_steps is an integer for n-step reachability for determining which nodes are exposed (default: 2-step)
def prep_seirds(dotfile, pkn, warning_steps, subgraph_dir):
  # Read PKN
  config = configparser.ConfigParser()
  config.read(pkn)
  vio_ed = [] ## vio_ed is list where each entry is an edge label (as str) that causes a violation
  recov_ed = {} ## recov_ed is dict: {'': eid}, where '' is an edge label (as str) and eid (str) is which violation it recovers (which edge/exploit ID)
  known_redirs = {} ## dict : {'' : eid}, where '' is an edge label (as str) that causes a known violation, also denoted by exploit ID eid
  num_exploits = config['General']['num_exploits']
  time_ed_int = [int(x) for x in (config['General']['time_ed'].replace(' ', '').split(','))] ## time_ed is a str where '' is an edge label that marks the step in time
  time_ed = set(time_ed_int)
  
  print("Parsing", num_exploits, "in the PKN.", flush = True)
  for e in range(int(num_exploits)):
    if config[("Exploit "+str(e))]['causes_vio'] == 'true':
      if(str(e) not in vio_ed):
        vio_ed.append(str(e))
    elif "redirect" in config[("Exploit "+str(e))]['causes_vio']:
      for redir in config[("Exploit "+str(e))]['causes_vio'].split("redirect")[1].split(","):
        if str(e) not in known_redirs:
          known_redirs = {str(e) : [redir]}
        elif str(e) not in known_redirs:
          known_redirs.update({str(e) : [redir]})
        else:
          known_redirs[str(e)] += [redir]

    if config[("Exploit "+str(e))]['recovery_edge'] != 'false':
      recov_ed.setdefault(str(e), [])
      recov_list =  [int(x) for x in config[("Exploit "+str(e))]['recovery_edge'].split(',')]
      for x in recov_list:
        recov_ed[str(e)].append(str(x))
  print("Parsed.", flush = True)

  if warning_steps == None:
    warning_steps = 2
  
  print("Using", warning_steps, "-step reachability for Exposure.", flush = True)
  G = nx.drawing.nx_agraph.read_dot(dotfile)
  #A = nx.drawing.nx_agraph.to_agraph(nx.drawing.nx_pydot.read_dot(dotfile))
  #A.layout('dot')
  #A.draw('tree.png')
  if(G.has_node('\\n')):
    G.remove_node('\\n') # Remove "newline" node from newline end of dot file
  #G=nx.DiGraph(A)
  
  print("Graph imported.", flush = True)
  # Get list of ALL exploits (edge labels) - only unique values
  elabels = nx.get_edge_attributes(G, "label") # keep ALL labels - will need them later
  exploits = list(set(elabels.values())) # only need the unique values for exploits
  exploits.sort() # don't really need to sort. doesn't matter.
  print("Gathered edge labels.", flush = True)
  
  # Create Subgraphs based on timesteps
  ## time_ed is a str where '' is an edge label that marks the step in time
  ## Assumes only one edge is a time transition
  time_nodes = [k[0] for k,v in elabels.items() if (int(v) in time_ed or str(v) in time_ed)]
  time_nodes = sorted(time_nodes, key=int)
  time_nodes.append(list(G)[len(G)-1])
  print("There are", len(time_nodes), "time transition steps in the graph.", flush = True)
  #upstream = '0'
  #time_step = 0
  SG_nodes = [['' for j in range(1)] for i in range(len(time_nodes))]
  print("Creating subgraphs for these...", flush = True)
  # Capitalize on known generator procedures:
  # 1. We know that node id's are assigned incrementally BEFORE exploration
  # 2. We know that with synchronous firing locks and flags, order is pre-known
  for node in G.nodes:
    for i in range(len(time_nodes)):
      tn = time_nodes[i]
      if (int(node) <= int(tn)):
        if not any(node in nest_list for nest_list in SG_nodes):
          SG_nodes[i].append(node)
          break
  
  SG_list = []
  for l in SG_nodes:
    l.remove('')
    SG = G.subgraph(l)
    SG_list.append(SG)
  
  print("Done.", flush = True)
    
  # Compartments - integer counts of how many nodes are in each compartment, NOT node ID's
  # Reminder: NO Dependencies between compartments: a node can simultaneously be infected and susceptible - compartments based on exploits
  # 2D - one for timesteps, one for Violation
  S = np.zeros(shape=(len(time_nodes), len(vio_ed)))
  E = np.zeros(shape=(len(time_nodes), len(vio_ed)))
  I = [[[''] for j in range(len(vio_ed))] for i in range(len(time_nodes))]# temporary - holding place until moved to R or D. Contains node ID's. Will eventually be overwritten with a count
  R = [[[''] for j in range(len(vio_ed))] for i in range(len(time_nodes))]
  D = np.zeros(shape=(len(time_nodes), len(vio_ed)))

  # Parameters
  ep_tmp = np.zeros(shape=(len(time_nodes), len(vio_ed))) # epsilon counter
  inf_ct = np.zeros(shape=(len(time_nodes), len(vio_ed))) # infection rate counter
  recov_ct = np.zeros(shape=(len(time_nodes), len(vio_ed))) # recov rate counter

  # Compartmentalize the subgraphs and derive transitionary parameters
  print("Compartmentalizing each subgraph and deriving their transitionary parameters...", flush = True)
  time_step = 0
  for SG in SG_list:
    filename = subgraph_dir + str(time_step) + ".gml"
    nx.write_gml(SG, filename)
    # Get list of SG exploits (edge labels) - only unique values
    SG_elabels = nx.get_edge_attributes(SG, "label") # keep ALL SG labels - will need them later
    
    exposed_sets = [set() for i in range(len(vio_ed))]
    infected_sets = [set() for i in range(len(vio_ed))]
    # Get Infected node IDs for each exploit
    ## vio_ed is list, where '' is an edge label (as str) and eid is the infection id (exploit id)
    for k,v in SG_elabels.items():
      for eid in vio_ed:
        ilist = []
        # The edge is an edge that causes a violation
        if (str(v) == str(eid) or int(v) == int(eid)):
          # Get Exposed
          epaths = findPaths(SG,k[1],warning_steps)
          for path in epaths:
            for node in path:
              #if node not in I[time_step][vio_ed.index(eid)]:
              if node not in infected_sets[vio_ed.index(eid)]:
                exposed_sets[vio_ed.index(eid)].update(node)
          # Check if Deceased
          if len(nx.descendants(SG, k[1])) == 0:
            D[time_step][vio_ed.index(eid)] += 1
          else:
            ilist.append(k[1])
            infected_sets[vio_ed.index(eid)].update(k[1])
        # The edge may cause a violation with a different label
        else:
          # Check if another exploit causes this exploit
          redir_bool = any(eid in sublist for sublist in known_redirs.values())
          if (redir_bool):
            # build redir list
            # Note: Multiple edges could redirect to this exploit - get all
            ed_redir_idx = [ i for i, x in enumerate(list(known_redirs.values())) if eid in x ]
            ed_redir_label = []
            for idx in ed_redir_idx:
              ed_redir_label.append(list(known_redirs.keys())[idx])
            if (str(v) in ed_redir_label):
              # Get Exposed
              epaths = findPaths(SG,k[1],warning_steps)
              for path in epaths:
                for node in path:
                  if node not in infected_sets[vio_ed.index(eid)]:
                    exposed_sets[vio_ed.index(eid)].update(node)
              # Check if Deceased
              if len(nx.descendants(SG, k[1])) == 0:
                D[time_step][vio_ed.index(eid)] += 1
              else:
                ilist.append(k[1])
                infected_sets[vio_ed.index(eid)].update(k[1])

        I[time_step][vio_ed.index(eid)].extend(ilist)
        # Clean up exposed set:
        exposed_sets[vio_ed.index(eid)] = exposed_sets[vio_ed.index(eid)] - infected_sets[vio_ed.index(eid)]
        #for i in ilist:
        #  if i in exposed_sets[vio_ed.index(eid)]:
        #    exposed_sets[vio_ed.index(eid)].remove(i)
      for ed, ei in recov_ed.items():
        if (str(v) == ed):
          for n in ei:
            R[time_step][vio_ed.index(n)].append(str(v))
    for i in range(len(exposed_sets)):
      E[time_step][i] += len(exposed_sets[i])
      
    if (time_step == len(SG_list)-1):
      print("Gathered all Infected, Exposed, Recovered, and Deceased nodes.", flush=True)
    
    # # Get Exposed nodes for each exploit using n-step (warning_step) reachability
    # # Additionally get Deceased nodes while we're looping through Infected
    # ## idx
    # for inodes in range(len(I[time_step])):
    #   ## infected nodes in exploit compartment (eid)
    #   e_set = set()
    #   for i in I[time_step][inodes]:
    #     # Get all nodes that can reach the infected nodes
    #     if i: # if string is not empty
    #       #e_dict = nx.shortest_path_length(SG,source=i)
    #       # reduce to the reachability step - exclude itself (0) and infected nodes
    #       #e_set.update([k for k,v in e_dict.items() if (v in range(1,int(warning_steps)) and v not in (item for sublist in I for item in sublist))])
    #       epaths = findPaths(SG,i,warning_steps)
    #       for path in epaths:
    #         for node in path:
    #           if node not in I[time_step][inodes]:
    #             e_set.update(node)
    #       if len(nx.descendants(SG, i)) == 0:
    #         D[time_step][inodes] += 1
    #   E[time_step][inodes] += len(e_set)
    # 
    # if (time_step == len(SG_list)-1):
    #   print("Gathered all Exposed and Deceased nodes.", flush=True)
    # 
    # # Get Recovered node IDs for each exploit
    # ## recov_ed is dict: {'': eid}, where '' is an edge label (as str) and eid is the infection id (exploit id) as str
    # for k,v in SG_elabels.items():
    #   for ed, ei in recov_ed.items():
    #     if (str(v) == ed):
    #       for n in ei:
    #         R[time_step][vio_ed.index(n)].append(str(v))
    # 
    # if (time_step == len(SG_list)-1):
    #   print("Gathered all Recovered nodes.", flush=True)
    # 
    # Susceptible: all remaining nodes
    #for v in range(len(vio_ed)):
      #S[time_step][v] = len(SG.nodes()) - ( (len(I[time_step][v])-1) + E[time_step][v] + (len(R[time_step][v])-1) + D[time_step][v] )
      #S[time_step] = len(SG.nodes()) - ( len(I[time_step][v]) + E[time_step] + len(R[time_step][v]) + D[time_step] )

  
    # Derive Parameters - using trivial weighting (based on num_edges)
    ## Recovery Counter
    for v in range(len(vio_ed)):
      # Susceptible: all remaining nodes
      S[time_step][v] = len(SG.nodes()) - ( len(I[time_step][v])-1 + E[time_step][v] + len(R[time_step][v])-1 + D[time_step][v] )
      for rnode in R[time_step][v]:
        if rnode:
          rnode_ct = len(SG.in_edges(rnode))
          if rnode_ct == 0:
            recov_ct[time_step][v] = 0
          else:
            recov_ct[time_step][v] += 1/rnode_ct
        
    ## Infection Counter
    for v in range(len(vio_ed)):
      for inode in I[time_step][v]:
        if inode:
          if len(SG.in_edges(inode)) == 0:
            # imported infection
            ep_tmp[time_step][v] += 1
          else:
            inf_ct[time_step][v] += 1/len(SG.in_edges(inode))
      I[time_step][v] = len(I[time_step][v])-1
      R[time_step][v] = len(R[time_step][v])-1

    time_step += 1
  # end SG loop
  print("Done.", flush = True)
  
  # Parameter Computation - NOT 2D - we want overall rates, but for each exploit
  beta = [0] * len(vio_ed)    # rate of infection
  delta = [1] * len(vio_ed)   # incubation period - how many steps until can be I
  gamma_r = [0] * len(vio_ed) # recovery rate
  gamma_d = [0] * len(vio_ed) # death rate
  mu = [0] * len(vio_ed)      # fatality ratio (NOT death rate - different.)
  epsilon = [0] * len(vio_ed) # infected import rate
  omega = [1] * len(vio_ed)   # waninig immunity rate - how many steps can beW between R and S

  for v in range(len(vio_ed)):
    for step in range(len(time_nodes)):
      beta[v] += inf_ct[step][v]/len(G)
      gamma_r[v] += recov_ct[step][v]/len(G)
      gamma_d[v] += D[step][v]/len(G)
      if int(D[step][v]) + int(R[step][v]) != 0:
        mu[v] += D[step][v]/(int(D[step][v]) + int(R[step][v]))
      epsilon += ep_tmp[step][v]/len(G)
  
  
  print('\n', flush = True)

  print("Model Parameters:", flush = True)
  for v in range(len(vio_ed)):
    ict = 0
    for i in range(len(inf_ct)):
      ict += inf_ct[i][v]
    print("Exploit ID", v, ":", flush = True)
    print("infect counter:", str(ict), flush = True)
    print("beta:", str(beta[v]), flush = True)
    print("delta:", str(delta[v]), flush = True)
    print("gamma_r:", str(gamma_r[v]), flush = True)
    print("gamma_d:", str(gamma_d[v]), flush = True)
    print("mu:", str(mu[v]), flush = True)
    print("epsilon:", str(epsilon[v]), flush = True)
    print("omega:", str(omega[v]), flush = True)
    print('\n', flush = True)

  return (S, E, I, R, D, beta, delta, gamma_r, gamma_d, mu, epsilon, omega, vio_ed)

def findPaths(G,u,n):
    if n==0:
        return [[u]]
    paths = [[u]+path for parent in G.predecessors(u) for path in findPaths(G,parent,n-1) if u not in path]
    return paths
