#!/bin/bash
for i in 5 50 100 150 200 250 300 350 400 450 500 550 601 650;
do
	cat ../${i}car.csv >> final_car.csv
done

sed -i '/Monetary,Time,DG,ROIM,ROIT/d' final_car.csv
sed -i 1i"Monetary,Time,DG,ROIM,ROIT" final_car.csv
