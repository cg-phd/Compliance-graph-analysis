#include <units.h>
#include <fstream>
#include <sstream>
#include <iterator>
#include <numeric>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional/optional.hpp>

#include "pkn.h"
#include "../util/util.h"

// Nodes 0->n should exist. There should be no missing nodes. The generator tool does not skip node IDs.
void ProblemSpace::prepare(std::string value_file, std::string known_e, std::string pkn_file, double num, std::string unit)
{
    // Read in values (priorities) generated from R centralities
    std::ifstream valfile(value_file, std::ios::in);
    if (valfile.is_open())
    {
        int node_id = 0;
        double sev = 0.0;
        while (valfile >> sev)
        {
            m_nodes.emplace_back(Node(node_id, sev));
            node_id++;
        }
    }
    else
    {
        std::cerr << "ERR: Priority Values file could not be opened." << std::endl;
        exit(-1);
    }

    // Read in the known exploits for each node that was obtained during priority computation
    std::string line, token;
    std::ifstream e_file(known_e, std::ios::in);
    int node_id = 1; // skip 0: it has no in_edges
    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(pkn_file, pt);

    std::set<int> parsed_e;

    std::cout << "Reading the known exploits file and the PKN..." << std::endl;
    while(std::getline(e_file, line))
    {
        if (node_id > (int) m_nodes.size()){
            std::cout << "Error: The number of nodes in the known_e file is greater than the nodes seen in the severity file. Exiting." << std::endl;
            exit(-1);
        }
        
        std::stringstream ss(line);
        while (std::getline(ss, token, ' ')){
            // We haven't seen this Exploit yet, read the pkn to obtain information about it
            if(!parsed_e.count(stoi(token)))
            {
                Exploit e(stoi(token));
                read_pkn(pt, e, num, unit);
                parsed_e.insert(stoi(token));
                m_nodes.at(node_id).add_exploit(e);
                m_exploits.insert(e);

                std::vector<double> e_dam = e.damages();
                double e_dam_sum = std::accumulate(e_dam.begin(), e_dam.end(), 0.0);

                std::vector<double> n_dam = m_nodes.at(node_id).damages();
                double n_dam_sum = std::accumulate(n_dam.begin(), n_dam.end(), 0.0);

                if (e_dam_sum > n_dam_sum)
                    m_nodes.at(node_id).damages(e_dam);

            }
            // We have seen this Exploit and already read the pkn. Just add it to this node, assuming it's not a duplicate.
            else{
                if(!m_nodes.at(node_id).exploits().count(stoi(token))){
                    m_nodes.at(node_id).add_exploit(*m_exploits.find(Exploit(stoi(token))));
                    Exploit e = *m_exploits.find(Exploit(stoi(token)));

                    std::vector<double> e_dam = e.damages();
                    double e_dam_sum = std::accumulate(e_dam.begin(), e_dam.end(), 0.0);

                    std::vector<double> n_dam = m_nodes.at(node_id).damages();
                    double n_dam_sum = std::accumulate(n_dam.begin(), n_dam.end(), 0.0);

                    if (e_dam_sum > n_dam_sum)
                        m_nodes.at(node_id).damages(e_dam);
                }
            }
        }
        node_id++;
    }

    std::cout << "Successfully read, processed, and imported " << node_id << " nodes and their exploits/mitigations." << std::endl;
}

void ProblemSpace::read_pkn(boost::property_tree::ptree pt, Exploit &e, double num, std::string unit)
{
    int eid = e.id();
    // Process e
    std::string e_name, e_mits;
    try{
        e_name = pt.get<std::string>("Exploit " + std::to_string(eid) + ".name");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'name' for Exploit " << std::to_string(eid)  << std::endl;
        std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
        exit(-1);
    }  

    e.name(e_name);

    // Damages
    /* Extrapolate Monetary Damages */
    double static_d, recurr_d, recurr_d_step;
    std::string recurr_d_unit;

    try{
        static_d = pt.get<double>("Exploit " + std::to_string(eid) +".static_dollar");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'static_dollar' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    }  

    try{
        recurr_d = pt.get<double>("Exploit " + std::to_string(eid) + ".recurr_dollar");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'static_dollar' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    } 
    
    try{
        recurr_d_step = pt.get<double>("Exploit " + std::to_string(eid) +".recurr_dollar_steps");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'recurr_dollar_steps' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    }

    try{
        recurr_d_unit = pt.get<std::string>("Exploit " + std::to_string(eid) +".recurr_dollar_units");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'recurr_dollar_units' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
        exit(-1);
    }

    double total_d = static_d;

    if (recurr_d != 0 && (recurr_d_unit != "NA" || recurr_d_unit != "na" || recurr_d_unit != "None" || recurr_d_unit != "none"))
        total_d += time_conversion(recurr_d_step, recurr_d_unit, num, unit);
    

    /* Extrapolate Time Costs */
    double static_td, recurr_td, recurr_td_step;
    std::string static_td_unit, recurr_td_unit;

    try{
        static_td = pt.get<double>("Exploit " + std::to_string(eid) +".static_time");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'static_time' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    }  

    try{
        recurr_td = pt.get<double>("Exploit " + std::to_string(eid) +".recurr_time");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'recurr_time' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    } 
    
    try{
        recurr_td_step = pt.get<double>("Exploit " + std::to_string(eid) +".recurr_time_steps");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'recurr_time_steps' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
        exit(-1);
    }

    try{
        static_td_unit = pt.get<std::string>("Exploit " + std::to_string(eid) +".static_time_units");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'static_time_units' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
        exit(-1);
    }

    try{
        recurr_td_unit = pt.get<std::string>("Exploit " + std::to_string(eid) +".recurr_time_units");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'recurr_time_steps' for Exploit " << std::to_string(eid) << std::endl;
        std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
        exit(-1);
    }

    double total_td = static_td;

    // Align units for static_t and recurr_t
    if (recurr_td != 0 && (recurr_td_unit != "NA" || recurr_td_unit != "na" || recurr_td_unit != "None" || recurr_td_unit != "none"))
    {
        total_td += time_conversion(recurr_td_step, recurr_td_unit, static_td, static_td_unit);
        // Extrapolate to desired time
        total_td += time_conversion(total_td, static_td_unit, num, unit);
    }

    std::vector<double> damages {total_d, total_td};
    e.damages(damages);

    // Mitigations
    try{
        e_mits = pt.get<std::string>("Exploit " + std::to_string(eid) + ".num_mits");
    }
    catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
        std::cerr << "Error reading 'num_mits' for Exploit " << std::to_string(eid)  << std::endl;
        std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
        exit(-1);
    }  

    int num_mits = stoi(e_mits);
    for(int j = 0; j < num_mits; j++)
    {
        /* Extrapolate Monetary Costs */
        double static_c, recurr_c, recurr_c_step;
        std::string recurr_c_unit;

        try{
            static_c = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".static_dollar");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'static_dollar' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        }  

        try{
            recurr_c = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_dollar");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'static_dollar' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        } 
        
        try{
            recurr_c_step = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_dollar_steps");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'recurr_dollar_steps' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        }

        try{
            recurr_c_unit = pt.get<std::string>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_dollar_units");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'recurr_dollar_units' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
            exit(-1);
        }

        double total_c = static_c;

        if (recurr_c != 0 && (recurr_c_unit != "NA" || recurr_c_unit != "na" || recurr_c_unit != "None" || recurr_c_unit != "none"))
            total_c += time_conversion(recurr_c_step, recurr_c_unit, num, unit);
        

        /* Extrapolate Time Costs */
        double static_t, recurr_t, recurr_t_step;
        std::string static_t_unit, recurr_t_unit;

        try{
            static_t = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".static_time");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'static_time' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        }  

        try{
            recurr_t = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_time");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'recurr_time' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        } 
        
        try{
            recurr_t_step = pt.get<double>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_time_steps");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'recurr_time_steps' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a double is provided for this key and try again." << std::endl;
            exit(-1);
        }

        try{
            static_t_unit = pt.get<std::string>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".static_time_units");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'static_time_units' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
            exit(-1);
        }

        try{
            recurr_t_unit = pt.get<std::string>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".recurr_time_units");
        }
        catch (boost::wrapexcept<boost::property_tree::ptree_bad_data>&){
            std::cerr << "Error reading 'recurr_time_steps' for Exploit " << std::to_string(eid) <<", Mitigation " << std::to_string(j) << std::endl;
            std::cerr << "Ensure a string is provided for this key and try again." << std::endl;
            exit(-1);
        }

        double total_t = static_t;

        // Align units for static_t and recurr_t
        if (recurr_t != 0 && (recurr_t_unit != "NA" || recurr_t_unit != "na" || recurr_t_unit != "None" || recurr_t_unit != "none"))
        {
            total_t += time_conversion(recurr_t_step, recurr_t_unit, static_t, static_t_unit);
            // Extrapolate to desired time
            total_t += time_conversion(total_t, static_t_unit, num, unit);
        }

        bool global = false;
        if (pt.get_child_optional("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".global"))
        {
            std::string tmp = pt.get<std::string>("Exploit" + std::to_string(eid) + "-Mitigation" + std::to_string(j)+".global");
            if (tmp == "true" || "True" || "TRUE" || "t" || "T")
                global = true;
        }
        std::vector costs {(int) std::round(total_c), (int) std::round(total_t)};
        Mitigation m(j, costs, global);
        e.add_mit(m);
    }
}

std::deque<std::vector<int>> ProblemSpace::get_weights()
{
    std::deque<std::vector<int>> weights;
    for (Node n : m_nodes){
        if (n.id() == 0)
            continue;

        for (Exploit e : n.exploits()){
            if (e.mits().size() == 0){
                std::vector<int> unpreventable = {m_num_constraints, __INT_MAX__};
                weights.emplace_back(unpreventable);
            }
            else{
                for (Mitigation m : e.mits()){
                    weights.emplace_back(m.costs());
                    if ((int) m.costs().size() > m_num_constraints)
                        m_num_constraints = m.costs().size();
                }
            }
        }
    }
            
    return weights;   
}

// For use after collapse (for use when there is 1 exploit with 1 mitigation)
std::deque<std::vector<int>> ProblemSpace::get_collapsed_weights()
{
    std::deque<std::vector<int>> weights;
    for (Node n : m_nodes)
    {
        if (n.id() == 0)
            continue;

        if (n.exploits().size() > 0)
        {
            if ((*(n.exploits().begin())).id() == -1)
            {
                if ((*(n.exploits().begin())).mits().at(0).id() == -1)
                {
                    // Get the -1 ID Exploit (Collapsed Exploit)'s single -1 ID Mitigation (Reduced Mitigation)'s costs
                    std::vector<int> costs = (*(n.exploits().begin())).mits().at(0).costs();
                    if ((int) costs.size() > m_num_constraints)
                        m_num_constraints = costs.size();

                    for (int i = 0; i < (int) costs.size(); i++)
                    {
                        if ((int) weights.size() <= i)
                        {
                            std::vector<int> c = {costs[i]};
                            weights.emplace_back(c);
                        }
                        else
                            weights.at(i).emplace_back(costs[i]);
                    }
                    //weights.emplace_back((*(n.exploits().begin())).mits().at(0).costs());
                }
                else
                {
                    std::cout << "ERR: Using 'get_collapsed_weights()' with an invalid Mitigation" << std::endl;
                    exit(-1);
                }
            }
            else
            {
                std::cout << "ERR: Using 'get_collapsed_weights()' with an invalid Exploit" << std::endl;
                exit(-1);
            }
        }
        // No error - this is normal behavior when iterating through collapsed Nodes
    }

    return weights;   
}

void ProblemSpace::collapse_exploits(std::vector<double> lambdas)
{
    for (Node &n : m_nodes)
    {
        n.collapse_exploits(lambdas, m_num_constraints);
    }  
}

void ProblemSpace::print() const
{
    for(const Node &n : m_nodes)
    {
        n.print();
    }
}

std::vector<double> ProblemSpace::get_severities()
{
    std::vector<double> values;
    for(Node n : m_nodes)
        values.emplace_back(n.severity()*1000000);
    return values;
}

std::vector<double> ProblemSpace::get_severities(double scale)
{
    std::vector<double> values;
    for(Node n : m_nodes)
        values.emplace_back(n.severity()*scale);
    return values;
}

double time_conversion(double from_num, std::string from_unit, double to_num, std::string to_unit)
{

    double total_t;

    if (to_unit == "S" || to_unit == "s" || to_unit == "Seconds" || to_unit == "seconds" || to_unit == "sec")
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::seconds>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::seconds>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::seconds>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::seconds>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::seconds>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::seconds>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }               
    else if (to_unit == "Min" ||to_unit== "min" ||to_unit== "Minutes" ||to_unit== "minutes" ||to_unit== "mins")
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::minutes>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::minutes>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::minutes>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::minutes>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::minutes>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::minutes>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }
    else if (to_unit == "H" ||to_unit== "h" ||to_unit== "Hours" ||to_unit== "hours" ||to_unit== "hr")
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::hours>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::hours>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::hours>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::hours>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::hours>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::hours>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }
    else if (to_unit == "D" ||to_unit== "d" ||to_unit== "Day" ||to_unit== "day" ||to_unit== "days")   
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::days>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::days>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::days>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::days>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::days>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::days>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }       
    else if (to_unit == "W" ||to_unit== "Weeks" ||to_unit== "weeks" ||to_unit== "w" ||to_unit== "wk" || to_unit == "week" || to_unit == "Week")
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::weeks>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::weeks>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::weeks>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::weeks>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::weeks>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::weeks>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }
    else if (to_unit == "Y" ||to_unit== "Years" ||to_unit== "years" ||to_unit== "y" || to_unit == "year")
    {
        if (from_unit == "S" || from_unit == "s" || from_unit== "Seconds" || from_unit== "seconds" || from_unit== "sec")
        {
            total_t = units::convert<units::time::seconds, units::time::years>(from_num) * to_num;
        }               
        else if (from_unit== "Min" ||from_unit== "min" ||from_unit== "Minutes" ||from_unit== "minutes" ||from_unit== "mins")
        {
            total_t = units::convert<units::time::minutes, units::time::years>(from_num) * to_num;
        }
        else if (from_unit== "H" ||from_unit== "h" ||from_unit== "Hours" ||from_unit== "hours" ||from_unit== "hr")
        {
            total_t = units::convert<units::time::hours, units::time::years>(from_num) * to_num;
        }
        else if (from_unit == "D" ||from_unit== "d" ||from_unit== "Day" ||from_unit== "day" ||from_unit== "days")   
        {
            total_t = units::convert<units::time::days, units::time::years>(from_num) * to_num;
        }       
        else if (from_unit == "W" ||from_unit== "Weeks" ||from_unit== "weeks" ||from_unit== "w" ||from_unit== "wk" || from_unit == "week" || from_unit == "Week")
        {
            total_t = units::convert<units::time::weeks, units::time::years>(from_num) * to_num;
        }
        else if (from_unit == "Y" ||from_unit== "Years" ||from_unit== "years" ||from_unit== "y" || from_unit == "year")
        {
            total_t = units::convert<units::time::years, units::time::years>(from_num) * to_num;
        }
        else
        {
            if (from_unit == "M" || from_unit == "Month" || from_unit == "Months" || from_unit == "month" || from_unit == "months" || from_unit == "mo")
            {
                std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
                exit(-1);
            }
            else{
                std::cerr << "Unknown 'from' conversion unit: " << from_unit << ". Please correct and try again." << std::endl;
                exit(-1);
            }
        }
    }
    else
    {
        if (to_unit == "M" || to_unit == "Month" || to_unit == "Months" || to_unit == "month" || to_unit == "months" || to_unit == "mo")
        {
            std::cerr << "Conversion with months is not allowed due to ambiguity. Please choose a different option and try again." << std::endl;
            exit(-1);
        }
        else
        {
            std::cerr << "Unknown 'to' conversion unit: " << to_unit << ". Please correct and try again." << std::endl;
            exit(-1);
        }
    }

    return total_t;
}
