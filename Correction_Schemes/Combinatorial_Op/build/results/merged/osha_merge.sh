#!/bin/bash
for i in 1 50 100 150 200 250 300 350 400 450 501 550 600 650;
do
	cat ../${i}osha.csv >> final_osha.csv
done

sed -i '/Monetary,Time,DG,ROIM,ROIT/d' final_osha.csv
sed -i 1i"Monetary,Time,DG,ROIM,ROIT" final_osha.csv
