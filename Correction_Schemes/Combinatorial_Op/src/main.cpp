#include <climits>
#include <iostream>
#include <fstream>
#include <sstream>

#include "pkn/pkn.h"
#include "mdkp/MDKP.h"
#include "mdkp/Relaxation.h"
#include "util/util.h"
#include <cmath>


void newtest(Relaxation &lr_space, ProblemSpace p_space, std::string f, bool flag, int m_cst)
{
    Knapsack lr_sack = lr_space.knap();

    std::string headers = "Monetary,Time,DG,ROIM,ROIT\n";
    std::ofstream csvfile;
    
    std::ifstream resume;
    resume.open(f);
    std::string line;
    std::string last_line;
    int numlines = 0;
    while(std::getline(resume,line, '\n')){ 
        last_line = line;
        numlines++;
    }
    bool header_flag = true;
    bool resume_flag = false;
    if(numlines > 0){
        header_flag = false;
        if(numlines > 1){
            resume_flag = true;
        }
    }
    resume.close();
    
    if (csvfile.is_open())
        csvfile.close();

    csvfile.open(f, std::ios::out | std::ios::app);
    
    if (header_flag)
        csvfile << headers << std::flush;
    else
        std::cout << "Ignoring headers." << std::endl;
    
    int t = 1000;
    int m = 500;
    if (flag)
        m = m_cst;

    resume_flag = false;
    if(resume_flag){
        std::istringstream ss(last_line);
        std::string substr;

        std::getline(ss, substr, ',');
        m = std::stoi(substr); 
        std::getline(ss, substr, ',');
        t = std::stoi(substr); 

        if(m==750 && t == 999){
            std::cout << "This example was already run to completion." << std::endl;
            return;
        }
        else if (t==999){
            t = 0;
            m += 1;
        }
        else{
            t += 1;
        }
        std::cout << "Resuming from previous run. m=" << m << ", and t=" << t << std::endl;
    }

    p_space.collapse_exploits(lr_space.lambdas());
    std::deque<std::vector<int>> orig_weights = p_space.get_collapsed_weights();
    std::vector<double> orig_vals = p_space.get_severities();

    std::vector<size_t> non_zero_val_idx;

    auto it = std::find_if(std::begin(orig_vals), std::end(orig_vals), [](double i){return i > 0;});
    while (it != std::end(orig_vals)) {
        non_zero_val_idx.emplace_back(std::distance(std::begin(orig_vals), it));
        it = std::find_if(std::next(it), std::end(orig_vals), [](double i){return i > 0.0;});
    }
    
    std::vector<double> nonzero_vals;
    std::deque<std::vector<int>> orig_nz_weights;
    std::vector<int> cst_one;
    std::vector<int> cst_two;

    for (size_t idx : non_zero_val_idx){
        nonzero_vals.emplace_back(orig_vals.at(idx));
        cst_one.emplace_back(orig_weights.at(0).at(idx));
        cst_two.emplace_back(orig_weights.at(1).at(idx));
    }

    orig_nz_weights.emplace_back(cst_one);
    orig_nz_weights.emplace_back(cst_two);


    std::deque<std::vector<int>> weights;
    std::vector<double> vals;

    for (m; m <750; m+=5)
    {
        for (t; t < 1005; t+=5)
        {
            std::cout << "M: " << m << " , T: " << t << std::endl;
            std::vector<int> csts = {m, t};
            lr_sack.cap_limits(csts);
         
            weights = orig_nz_weights;
            vals = nonzero_vals;

            std::vector<double> z_lams = {0.0 , 0.0};

            LR_solve(lr_sack, vals, weights, lr_space.lambdas());

            //std::cout << "Obtained Upper Bound: " << lr_sack.value() << std::endl;
            std::set<int> lr_nodes = lr_sack.node_ids();
            std::cout << "Nodes: " << lr_nodes << std::endl;
            int real_val = 0;
            std::vector<double> damages = {0, 0};
            std::vector<int> real_costs = {0, 0};

            for (int n : lr_nodes){
                int translated_n = non_zero_val_idx[n];
                for (int i =0; i < 2; i++){
                    real_costs[i] += weights[i][translated_n];
                    damages[i] += p_space.nodes().at(translated_n).damages()[i];
                    //max_damages[i] += p_space.nodes().at(n).min_damages()[i];
                }
                
                real_val += vals[translated_n];
            }

            //std::cout << "Real Knapsack Value: " << real_val << std::endl;

            float power_of_10 = std::pow(10, 7);
            double dual_gap = 0;
            if (real_val != 0){
                dual_gap = static_cast<double>((lr_sack.value() - real_val)/static_cast<double>(real_val));
                dual_gap = std::round(dual_gap * power_of_10) / power_of_10;
            }

            //std::cout << "Duality Gap: " << dual_gap << std::endl;

            //std::cout << "Costs: " << real_costs << std::endl;
            //std::cout << "Caps: " << lr_sack.cap_limits() << std::endl;
            //std::cout << "Min Damages: " << min_damages << std::endl;
            //std::cout << "Max Damages: " << max_damages << std::endl;
            double ROIM = 0;
            if (real_costs[0] != 0)
                ROIM = (damages[0] - real_costs[0]) / real_costs[0];
            
            double ROIT = 0;
            if (real_costs[1] != 0)
                ROIT = (damages[1] - real_costs[1]) / real_costs[1];

            std::cout << "Real Costs: " << real_costs << std::endl;
            std::cout << "Damages: " << damages << std::endl;
            std::cout << std::to_string(m) + "," + std::to_string(t) + "," + std::to_string(dual_gap) + "," + std::to_string(ROIM) + "," + std::to_string(ROIT) << std::endl;
            csvfile << std::to_string(m) + "," + std::to_string(t) + "," + std::to_string(dual_gap) + "," + std::to_string(ROIM) + "," + std::to_string(ROIT) +"\n" << std::flush;  
            lr_space.reset();

      
        }
        t = 0;
    }

    csvfile.close();
   
    
}

void cartest(Relaxation &lr_space, ProblemSpace p_space)
{
    p_space.collapse_exploits(lr_space.lambdas());
    std::deque<std::vector<int>> orig_weights = p_space.get_collapsed_weights();
    std::vector<double> orig_vals = p_space.get_severities();

    std::vector<size_t> non_zero_val_idx;

    auto it = std::find_if(std::begin(orig_vals), std::end(orig_vals), [](double i){return i > 0;});
    while (it != std::end(orig_vals)) {
        non_zero_val_idx.emplace_back(std::distance(std::begin(orig_vals), it));
        it = std::find_if(std::next(it), std::end(orig_vals), [](double i){return i > 0.0;});
    }
    
    std::vector<double> nonzero_vals;
    std::deque<std::vector<int>> orig_nz_weights;
    std::vector<int> cst_one;
    std::vector<int> cst_two;

    for (size_t idx : non_zero_val_idx){
        nonzero_vals.emplace_back(orig_vals.at(idx));
        cst_one.emplace_back(orig_weights.at(0).at(idx));
        cst_two.emplace_back(orig_weights.at(1).at(idx));
    }

    orig_nz_weights.emplace_back(cst_one);
    orig_nz_weights.emplace_back(cst_two);


    std::deque<std::vector<int>> weights;
    std::vector<double> vals;

    weights = orig_nz_weights;
    vals = nonzero_vals;

    std::vector<double> z_lams = {0.0 , 0.0};


    //LR_solve(lr_sack, vals, weights, lr_space.lambdas());
    int lr_ctr = 0;
    Knapsack sack = lr_space.knap();
    //while (!lr_space.stop() && !lr_space.knap().op())
    while(((lr_space.lambdas()[0] - lr_space.last_lambdas()[0]) < lr_space.tol()) || !sack.op())
    {
        lr_space.knap().reset();
        std::deque<std::vector<int>> iter_nz_weights;
        std::vector<double> LRvals; 


        if (lr_ctr == 0){
            iter_nz_weights = weights;
            LRvals = vals;
        }

        else{
            std::vector<int> iter_cst_one;
            std::vector<int> iter_cst_two;

            //p_space.collapse_exploits(lr_space.lambdas());
            std::deque<std::vector<int>> iter_weights = p_space.get_collapsed_weights();

            for (size_t idx : non_zero_val_idx){
                int wtz = iter_weights.at(0).at(idx);
                if (wtz < 0)
                    wtz = 0;
                int wto = iter_weights.at(1).at(idx);
                if (wto < 0)
                    wto = 0;
                iter_cst_one.emplace_back(wtz);
                iter_cst_two.emplace_back(wto);
            }

            iter_nz_weights.emplace_back(iter_cst_one);
            iter_nz_weights.emplace_back(iter_cst_two);

            //LRvals = LR(vals, iter_nz_weights, lr_space.lambdas());
        }

        //Iterate: LR vals, weights, original sevs
        
        if (lr_space.loop_count() % 5 == 0){
            std::cout << "Iteration " << lr_space.loop_count() + 1 << "..." << std::endl;
            std::cout << "Current lambdas: " << lr_space.lambdas() << std::endl;
            //std::cout << "Second dim weights: " <<iter_nz_weights.at(1) << std::endl;
            //std::cout << "Current LR vals: " << LRvals << std::endl;
        }
        
        //iter_nz_weights.pop_front();
        lr_space.test_LR_solve(vals, iter_nz_weights);
     
        int real_val = 0;
        std::vector<double> damages = {0, 0};
        std::vector<int> real_costs = {0, 0};
        std::vector<int> real_n;

        for (int n : lr_space.knap().node_ids()){
            int translated_n = non_zero_val_idx[n];
            for (int i =0; i < 2; i++){
                real_costs[i] += orig_weights[i][translated_n];
                if(i==1)
                    real_n.emplace_back(translated_n);
                //damages[i] += p_space.nodes().at(translated_n).damages()[i];
                //max_damages[i] += p_space.nodes().at(n).min_damages()[i];
            }
        
            real_val += vals[translated_n];
        }
        std::cout << "Real Costs: " << real_costs << std::endl;
        std::cout << "Real Val: " << real_val << std::endl;
        std::cout << "LR Val: " << lr_space.knap().value() << std::endl;
        std::cout << "Real Knapsack Nodes: " << real_n << std::endl;
        //lr_space.iterate(LRvals, iter_nz_weights, vals);
            
        lr_ctr++;
    }

    std::cout << "Finalized LR loop" << std::endl;
    Knapsack lr_sack = lr_space.knap();
    //std::cout << "Obtained Upper Bound: " << lr_sack.value() << std::endl;
    std::set<int> lr_nodes = lr_sack.node_ids();
    int real_val = 0;
    std::vector<double> damages = {0, 0};
    std::vector<int> real_costs = {0, 0};

    for (int n : lr_nodes){
        int translated_n = non_zero_val_idx[n];
        for (int i =0; i < 2; i++){
            real_costs[i] += orig_weights[i][translated_n];
            damages[i] += p_space.nodes().at(translated_n).damages()[i];
            //max_damages[i] += p_space.nodes().at(n).min_damages()[i];
        }
        
        real_val += vals[translated_n];
    }

    //std::cout << "Real Knapsack Value: " << real_val << std::endl;

    float power_of_10 = std::pow(10, 7);
    double dual_gap = 0;
    if (real_val != 0){
        dual_gap = static_cast<double>((lr_sack.value() - real_val)/static_cast<double>(real_val));
        dual_gap = std::round(dual_gap * power_of_10) / power_of_10;
    }

    //std::cout << "Duality Gap: " << dual_gap << std::endl;

    //std::cout << "Costs: " << real_costs << std::endl;
    //std::cout << "Caps: " << lr_sack.cap_limits() << std::endl;
    //std::cout << "Min Damages: " << min_damages << std::endl;
    //std::cout << "Max Damages: " << max_damages << std::endl;
    double ROIM = 0;
    if (real_costs[0] != 0)
        ROIM = (damages[0] - real_costs[0]) / real_costs[0];
    
    double ROIT = 0;
    if (real_costs[1] != 0)
        ROIT = (damages[1] - real_costs[1]) / real_costs[1];

    std::cout << "Dual Gap: " + std::to_string(dual_gap) + ", ROIM: " + std::to_string(ROIM) + ", ROIT: " + std::to_string(ROIT) << std::endl;
    lr_space.reset();


}
    
void optimize(Relaxation &lr_space, ProblemSpace p_space)
{
    Knapsack lr_sack = lr_space.knap();
    p_space.collapse_exploits(lr_space.lambdas());
    std::deque<std::vector<int>> weights = p_space.get_collapsed_weights();
    // Weights is a deque of vectors: each vector is 1D, index represents node. Ex: weights[0][0] is 0th cost type of the 0th node

    double best_val = __INT_MAX__;
    std::set<int> best_nodes; // Trackers for knowing when to update exploit set

    // Lock each constraint, but only up to what we have available
    for(int w_it = 0; w_it < p_space.num_constraints()-1; w_it++)
    {
        //Rotate weights (shift right)
        if (w_it != 0)
        {       
            std::vector<int> w_idx = weights.at(weights.size()-1);
            weights.erase(weights.end());
            weights.emplace_front(w_idx);
            lr_sack.rotate_caps();
        }

        std::vector<int> w = weights.front(); // first weight is left unaffected by LR
        //weights.pop_front();

        std::deque<std::vector<int>> iter_weights = weights;

        while (!lr_space.stop() && !lr_space.knap().op())
        {
            if (lr_space.loop_count() % 5 == 0){
                std::cout << "Iteration: " << lr_space.loop_count() + 1 << "..." << std::endl;
                std::cout << "Current lambdas: " << lr_space.lambdas() << std::endl;
            }
            
            //iter_weights.pop_front();
            std::vector<double> antisevs = p_space.get_severities();
            std::vector<double> vals = LR(p_space.get_severities(), iter_weights, lr_space.lambdas());
            //iter_weights.emplace_front(w);
            lr_space.iterate(vals, weights, antisevs);

            if (lr_space.knap().value() != best_val || lr_space.knap().node_ids() != best_nodes)
            {
                best_nodes.clear(); // Value or nodes changed - get our new Mitigations
                for (int n : lr_space.knap().node_ids())
                {
                    lr_space.mitigated_exploits(*(p_space.nodes().at(n).exploits().begin()));
                }
            }

            // Collapse with new lambdas and get weights
            p_space.collapse_exploits(lr_space.lambdas());
            iter_weights = p_space.get_collapsed_weights();
            
            //tmp_w.pop_front();
            //iter_weights = tmp_w;

            // Remove the locked constraint. Don't need to rotate caps -> already done in the wrapped for loop
            // Rotate weights (shift right) in a hacky way -> just shift the number of times we're supposed to
            //for(int i = 0; i < w_it; i++)
            //{       
            //    std::vector<int> w_idx = iter_weights.at(iter_weights.size()-1);
            //    iter_weights.erase(iter_weights.end());
            //    iter_weights.emplace_front(w_idx);
            //}

            //iter_weights.pop_front();
        }

        std::cout << "Exploring next constraint." << std::endl;
        lr_space.stop(false);
        //lr_space.knap().reset();
    }
    
}

// Check global mitigations. Add nodes if they can be solved with current mitigation strategies
std::tuple<double, double> post_process(Relaxation &lr_space, ProblemSpace p_space)
{
    double lr_val = 0.0;
    double real_val = 0.0;
    std::vector<double> sevs = p_space.get_severities();
    /*
    for (Node n : p_space.nodes())
    {
        if (n.exploits().size() > 0)
        {
            Exploit e = *(n.exploits().begin());
            bool is_mitigated = lr_space.mitigated_exploits().find(e) != lr_space.mitigated_exploits().end();
            if (is_mitigated && (e.mits().size() > 0 && e.mits().front().is_global_mit()))
            {
                //std::cout << "Adding Node ID " << n.id() << std::endl;
                lr_space.knap().add_item(n.id());
                real_val += sevs.at(n.id());
                //std::cout << "It has a value of " << sevs.at(n.id()) << std::endl;
                lr_val += LR({sevs.at(n.id())}, {e.mits().front().costs()}, lr_space.best_lambdas()).at(0);
                //std::cout << "It has a LR value of " << LR({sevs.at(n.id())}, {e.mits().front().costs()}, lr_space.best_lambdas()).at(0) << std::endl;
                //e.print();
            }   

        }

    }
    */
    
    std::cout << "LR Repair: " << lr_val/100000 << std::endl;
    std::cout << "Improving real val by: " << real_val/100000 << std::endl;
    return std::tie(lr_val, real_val);
}


void print_results(Relaxation &lr_space, ProblemSpace p_space, std::pair<double, std::string> timeframe)
{
    double real_val = 0;
    std::vector<double> vals = p_space.get_severities();
    std::vector<int> total_costs = {p_space.num_constraints(), 0};

    for (int n : lr_space.best_nodes()){
        vecAdd(total_costs, p_space.nodes().at(n).costs());
        real_val += vals.at(n)/1000000;
    }
    
    std::tuple<double, double> repair = post_process(lr_space, p_space);
    double actual_lr = (lr_space.ub() + std::get<0>(repair))/1000000;
    double actual_val = real_val + std::get<1>(repair)/1000000;

    std::cout << "Obtained Upper Bound: " << actual_lr << std::endl;
    std::cout << "Mitigated Nodes in LR Knapsack: " << lr_space.best_nodes() << std::endl;

    //std::set<int> lr_nodes = lr_space.knap().node_ids();

    std::cout << "Real Knapsack Value: " << actual_val << std::endl;
    std::cout << "Duality Gap: " << static_cast<double>(((actual_lr - actual_val))/static_cast<double>(actual_val)) << std::endl;
    std::cout << std::endl;

    std::cout << "Total costs over " << timeframe.first << " " << timeframe.second << ":" << std::endl;
    int i = 0;
    for (int c : total_costs)
    {
        std::cout << "Cost " << i << ": " << c << " (" << 100*(double(c)/(double) lr_space.knap().cap_limits()[i]) << "% of alloted budget)" << std::endl;
        i++;
    }    
}

int automobile_example(int m_cst)
{
    //Provide files
    std::string value_file = "../../../Examples/Automobile_Maintenance/Full_EX/prio.txt";
    std::string e_file = "../../../Examples/Automobile_Maintenance/Full_EX/known_e.txt";
    std::string pkn_file = "../../../Examples/Automobile_Maintenance/Full_EX/car_pkn.ini";

    //Provide desired extrapolation
    double num = 1;
    std::string unit = "year";

    //Provide constraints
    std::vector<int> automobile_constraints = {600, 12};

    //Create the problem space by reading, parsing, and extrapolating all necessary files
    ProblemSpace p_space;
    p_space.prepare(value_file, e_file, pkn_file, num, unit);

    //Create the knapsack with the provided constraints
    Knapsack lr_sack(automobile_constraints);

    //Create a relaxation object to interface with the different components
    Relaxation lr_space(lr_sack);
    
    //std::cout << std::endl;
    //std::cout << "Performing Combinatorial Optimization." << std::endl;
    optimize(lr_space, p_space);
    //cartest(lr_space, p_space);

    std::string fname = "car.csv";
    bool flag = false;
    if (m_cst != 600){
        fname = std::to_string(m_cst) + fname;
        flag = true;
    }
    //newtest(lr_space, p_space, fname, flag, m_cst);

    //std::cout << "Complete after " << lr_space.loop_count() << " iterations. Printing results." << std::endl;
    //std::cout << std::endl;

    print_results(lr_space, p_space, std::make_pair(num, unit));

    return 0;
}

int hipaa_example(int m_cst)
{
    //Provide files
    std::string value_file = "../../../Examples/HIPAA/Full_EX/prio.txt";
    std::string e_file = "../../../Examples/HIPAA/Full_EX/known_e.txt";
    std::string pkn_file = "../../../Examples/HIPAA/Full_EX/hipaa_pkn.ini";

    //Provide desired extrapolation
    double num = 1;
    std::string unit = "year";

    //Provide constraints
    // 500k/yr, 1000hr/yr
    std::vector<int> hipaa_constraints = {500, 1000};

    //Create the problem space by reading, parsing, and extrapolating all necessary files
    ProblemSpace p_space;
    p_space.prepare(value_file, e_file, pkn_file, num, unit);
    //p_space.print();

    //Create the knapsack with the provided constraints
    Knapsack lr_sack(hipaa_constraints);
    //lr_sack.print();

    //Create a relaxation object to interface with the different components
    Relaxation lr_space(lr_sack);
    
    std::cout << std::endl;
    std::cout << "Performing Combinatorial Optimization." << std::endl;
    optimize(lr_space, p_space);
    //std::cout << "Complete after " << lr_space.loop_count() << " iterations. Printing results." << std::endl;
    //std::cout << std::endl;
    print_results(lr_space, p_space, std::make_pair(num, unit));
    std::string fname = "hipaa.csv";
    bool flag = false;
    if (m_cst != 500){
        fname = std::to_string(m_cst) + fname;
        flag = true;
    }

    //newtest(lr_space, p_space, fname, flag, m_cst);

    return 0;
}

int osha_example(int m_cst)
{
    //Provide files
    std::string value_file = "../../../Examples/OSHA_1910H/Full_EX/prio.txt";
    std::string e_file = "../../../Examples/OSHA_1910H/Full_EX/known_e.txt";
    std::string pkn_file = "../../../Examples/OSHA_1910H/Full_EX/osha_pkn_div100.ini";

    //Provide desired extrapolation
    double num = 1;
    std::string unit = "year";

    //Provide constraints
    // 500k/yr, 1000hr/yr
        std::vector<int> hipaa_constraints = {500, 250};
    //Create the problem space by reading, parsing, and extrapolating all necessary files
    ProblemSpace p_space;
    p_space.prepare(value_file, e_file, pkn_file, num, unit);
    //p_space.print();

    //Create the knapsack with the provided constraints
    Knapsack lr_sack(hipaa_constraints);
    //lr_sack.print();

    //Create a relaxation object to interface with the different components
    Relaxation lr_space(lr_sack);
    
    std::cout << std::endl;
    //std::cout << "Performing Combinatorial Optimization." << std::endl;
    std::string fname = "osha.csv";
    bool flag = false;
    if (m_cst != 500){
        fname = std::to_string(m_cst) + fname;
        flag = true;
    }
    newtest(lr_space, p_space, fname, flag, m_cst);

    //optimize(lr_space, p_space);
    //std::cout << "Complete after " << lr_space.loop_count() << " iterations. Printing results." << std::endl;
    //std::cout << std::endl;
    //print_results(lr_space, p_space, std::make_pair(num, unit));

    return 0;
}

int main(int argc, char *argv[])
{
    //Q: How to handle difference in magnitude between severity and weights?
    //EX: Severity=10, W=100. LR Says value will be 10-(lambda*W).
    //Q: Are units a concern?
    //EX: 60s == 1m, but magnitude is different
    //Q: Can we introduce a "Mitigation Value" that is consistent across all mitigations given constraints?
    //Q: CAN we even use LR? How do we "optimize" lambda? How do we know which constraint to relax? Does it not inherently favor one over the other?

    //0. Init lambda to 0
    //1. Perform LR on the mits of each node to find the single best mitigation. TODO: Analyze duality gap/similar for this type of relaxation
    //2. Collapse nodes for globally preventable mits
    //3. Knapsack
    //4. Repeat 0-4 with new lambda until condition met
    if(argc > 0 )
    {
        if (std::string(argv[1]) == "car"){
            std::cout << "/********** Running Automobile Example **********/" << std::endl;
            if (argc > 2)
                automobile_example(std::stoi(argv[2]));
            else
                automobile_example(600);
        }
        else if (std::string(argv[1]) == "hipaa"){
            std::cout << "/********** Running HIPAA Example **********/" << std::endl;
            if (argc > 2)
                hipaa_example(std::stoi(argv[2]));
            else
                hipaa_example(500);
        }
        else if (std::string(argv[1]) == "osha"){
            std::cout << "/********** Running OSHA Example **********/" << std::endl;
            if (argc > 2)
                osha_example(std::stoi(argv[2]));
            else
                osha_example(500);
        }
        else if (std::string(argv[1]) == "all"){
            std::cout << "Running all examples." << std::endl;

            std::cout << "/********** Running Automobile Example **********/" << std::endl;
            automobile_example(600);

            std::cout << "/********** Running HIPAA Example **********/" << std::endl;
            hipaa_example(500);

            std::cout << "/********** Running OSHA Example **********/" << std::endl;
            osha_example(500);
        }
        else{
            std::cout << "Unknown input." << std::endl;
            exit(-1);
        }
            
        
    }
    else{
        std::cout << "/********** Running Automobile Example **********/" << std::endl;
        automobile_example(600);

        //std::cout << "/********** Running HIPAA Example **********/" << std::endl;
        //hipaa_example();
    }

    return 0;
}