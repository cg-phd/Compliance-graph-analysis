graph [
  directed 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "1901"
  ]
  node [
    id 1
    label "1900"
  ]
  node [
    id 2
    label "1906"
  ]
  node [
    id 3
    label "1905"
  ]
  node [
    id 4
    label "1902"
  ]
  node [
    id 5
    label "1903"
  ]
  node [
    id 6
    label "1904"
  ]
  edge [
    source 1
    target 2
    label "21"
  ]
  edge [
    source 3
    target 2
    label "21"
  ]
  edge [
    source 4
    target 2
    label "21"
  ]
  edge [
    source 5
    target 2
    label "21"
  ]
  edge [
    source 6
    target 2
    label "21"
  ]
]
