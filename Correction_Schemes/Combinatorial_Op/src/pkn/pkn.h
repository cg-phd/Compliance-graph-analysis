#pragma once

#include <deque>
#include <set>
#include <boost/property_tree/ptree.hpp>

#include "Node.h"

class ProblemSpace
{
    public:
        ProblemSpace():
            m_num_constraints { 0 }
        {}

        // Read the R centralities file, AG/CG Graphviz dotfile, prior-knowledge network, and extrapolate to a given length of time to create our Problem Space
        void prepare(std::string value_file, std::string dotfile, std::string pkn_file, double num, std::string unit);
                
        // Compile a deque of vectors that contains all the weights from the Mitigations
        std::deque<std::vector<int>> get_weights();
        std::deque<std::vector<int>> get_collapsed_weights();

        std::deque<Node> nodes() const { return m_nodes; }
        void nodes(std::deque<Node> n) { m_nodes = n; }
        std::set<Exploit> exploits() const { return m_exploits; }

        int num_constraints() const { return m_num_constraints; }
        void num_constraints(int n) { m_num_constraints = n; }

        // Compile a vector of all the Nodes' values - with or without scaling
        std::vector<double> get_severities();
        std::vector<double> get_severities(double scale);

        // Reduce all mits from an exploit into a single mit, and then combine all exploits from a node into a single exploit
        void collapse_exploits(std::vector<double> lambdas); 
        void print() const;

    private:
        // Helper function - reads the prior-knowledge network
        void read_pkn(boost::property_tree::ptree pt, Exploit &e, double num, std::string unit);
        std::deque<Node> m_nodes;
        std::set<Node> m_collapsed_nodes;
        std::set<Exploit> m_exploits;
        int m_num_constraints;
};

double time_conversion(double from_num, std::string from_unit, double to_num, std::string to_unit);

