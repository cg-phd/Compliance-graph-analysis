#!/usr/bin/python3 

import configparser

def ALE(vio_id, pkn, delta, keytype):
  config = configparser.ConfigParser()
  config.read(pkn)
  
  if (keytype == "monetary"):
    key = "static_dollar"
  elif (keytype == "time"):
    key = "static_time"
      
  SLE = float(config[("Exploit "+str(vio_id))][key])

  return (SLE * delta)
