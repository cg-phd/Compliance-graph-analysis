#include "util.h"
#include <algorithm>
#include <cmath>

int knap_max(int a, int b) { return (a > b) ? a : b; }

void vecMult(std::vector<int> &v, double k){
    std::transform(v.begin(), v.end(), v.begin(), [k](int &c){ return std::round(c*k); });
    //std::transform(v.begin(), v.end(), v.begin(), [k](double &c){ return c*k; });

}


void vecDiv(std::vector<int> &v, double k){
    std::transform(v.begin(), v.end(), v.begin(), [k](int &c){ return std::round(c/k); });
    //std::transform(v.begin(), v.end(), v.begin(), [k](double &c){ return c*k; });

}

void vecAdd(std::vector<int> &a, std::vector<int> b)
{
    if (a.size() > b.size())
        for(int i = 0; i < (int) b.size(); i++)
            a[i] += b[i];
    else
    {
        for(int i = 0; i < (int) a.size(); i++)
            a[i] += b[i];
        for(int i = (int) a.size(); i < (int) b.size(); i++)
                a.emplace_back(b[i]);
    }
}