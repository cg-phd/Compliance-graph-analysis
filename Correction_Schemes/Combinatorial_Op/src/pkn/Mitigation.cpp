#include <climits>
#include <iostream>

#include "Mitigation.h"
#include "../util/util.h"

void Mitigation::print() const{
    std::cout << "    Mitigation ID: " << m_id << std::endl;
    std::cout << "      Costs: " << m_costs << std::endl;
    std::cout << "      Global: " << m_is_global << std::endl;
}

double Mitigation::lr(int val, std::vector<double> lambdas)
{
    // No penalty for first cost (lambda = 1) => cost_sum is just the locked cost
    double cost_sum = m_costs[0];

    // Skip first cost - no associated lambda per LR
    if (m_costs.size() > 1 )
    {
        for (int i = 1; i < (int) m_costs.size(); i++)
        {   
                cost_sum += (m_costs[i]*lambdas[i-1]);
        }
    }
    
    return (val - cost_sum);
}

