function plot_data2(M,D,Pareto)
% This function to plot Pareto solution
% symbol = ['o','s','^','v','<','>','d','p'];
% colors = ['b','g','r','c','m','y','k','b'];
pl_data= Pareto(:,D+1:D+M); % extract data to plot
pl_data=sortrows(pl_data,2);
X=pl_data(:,1);
Y=pl_data(:,2);
figure;
scatter(X, Y,'*');
% Add title and axis labels
title('Optimal Solution Pareto Set for a Constant-Based Cost Model of a Compliance Graph');
xlabel('Time Cost');
ylabel('Monetary Cost');
grid;
% Add a colorbar with tick labels
end

