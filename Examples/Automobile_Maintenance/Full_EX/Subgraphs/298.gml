graph [
  directed 1
  name "G"
  graph [
  ]
  node [
    id 0
    label "1945"
  ]
  node [
    id 1
    label "1965"
  ]
  node [
    id 2
    label "1957"
  ]
  node [
    id 3
    label "1954"
  ]
  node [
    id 4
    label "1942"
  ]
  node [
    id 5
    label "1962"
  ]
  node [
    id 6
    label "1970"
  ]
  node [
    id 7
    label "1974"
  ]
  node [
    id 8
    label "1967"
  ]
  node [
    id 9
    label "1971"
  ]
  node [
    id 10
    label "1941"
  ]
  node [
    id 11
    label "1943"
  ]
  node [
    id 12
    label "1956"
  ]
  node [
    id 13
    label "1969"
  ]
  node [
    id 14
    label "1948"
  ]
  node [
    id 15
    label "1949"
  ]
  node [
    id 16
    label "1972"
  ]
  node [
    id 17
    label "1947"
  ]
  node [
    id 18
    label "1961"
  ]
  node [
    id 19
    label "1964"
  ]
  node [
    id 20
    label "1953"
  ]
  node [
    id 21
    label "1952"
  ]
  node [
    id 22
    label "1951"
  ]
  node [
    id 23
    label "1944"
  ]
  node [
    id 24
    label "1975"
  ]
  node [
    id 25
    label "1968"
  ]
  node [
    id 26
    label "1950"
  ]
  node [
    id 27
    label "1958"
  ]
  node [
    id 28
    label "1959"
  ]
  node [
    id 29
    label "1966"
  ]
  node [
    id 30
    label "1963"
  ]
  node [
    id 31
    label "1960"
  ]
  node [
    id 32
    label "1946"
  ]
  node [
    id 33
    label "1973"
  ]
  node [
    id 34
    label "1976"
  ]
  node [
    id 35
    label "1955"
  ]
  edge [
    source 1
    target 25
    label "21"
  ]
  edge [
    source 8
    target 25
    label "21"
  ]
  edge [
    source 19
    target 25
    label "21"
  ]
  edge [
    source 28
    target 25
    label "21"
  ]
  edge [
    source 29
    target 25
    label "21"
  ]
]
