import pandas as pd

car_df = pd.read_csv('car_ft.csv', header=0)
osha_df = pd.read_csv('osha_ft.csv', header=0)
hip_df = pd.read_csv('hipaa_ft.csv', header=0)

print("Automobile Rows:", len(car_df.axes[0]))
print("Automobile Cols:", len(car_df.axes[1]))
print("Automobile Total Cells:", len(car_df.axes[0]) * len(car_df.axes[1]))
num = 0

for i in car_df.columns:
   num += car_df[i][car_df[i]==0].count()
print("Automobile Total Number of 0s:", num/(len(car_df.axes[0]) * len(car_df.axes[1])))

print("HIPAA Rows:", len(hip_df.axes[0]))
print("HIPAA Cols:", len(hip_df.axes[1]))
print("HIPAA Total Cells:", len(hip_df.axes[0]) * len(hip_df.axes[1]))
num = 0

for i in hip_df.columns:
   num += hip_df[i][hip_df[i]==0].count()
print("HIPAA Total Number of 0s:", num/(len(hip_df.axes[0]) * len(hip_df.axes[1])))

print("OSHA Rows:", len(osha_df.axes[0]))
print("OSHA Cols:", len(osha_df.axes[1]))
print("OSHA Total Cells:", len(osha_df.axes[0]) * len(osha_df.axes[1]))
num = 0

for i in osha_df.columns:
   num += osha_df[i][osha_df[i]==0].count()
print("OSHA Total Number of 0s:", num/(len(osha_df.axes[0]) * len(osha_df.axes[1])))